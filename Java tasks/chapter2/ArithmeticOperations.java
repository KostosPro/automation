package chapter2;

import java.util.Scanner;

public class ArithmeticOperations{

	public static void main (String[] args){
		
		Scanner input = new Scanner(System.in);
		
		int firstInt;
		int secondInt;
				
		System.out.println("Please enter first integer");
		firstInt = input.nextInt();
		
		System.out.println("Please enter second integer");
		secondInt = input.nextInt();
		
		
		int sum = firstInt + secondInt;
		System.out.printf("%s = %d%n", "sum", sum);
		
		int product = firstInt * secondInt;
		System.out.printf("%s = %d%n", "produc", product);
		
		int difference = firstInt - secondInt;
		System.out.println("difference = " + difference);
		
		int division = firstInt / secondInt;
		System.out.println("division = " + division);
	
	}
}
