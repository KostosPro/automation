package chapter2;

import java.util.Scanner;

public class SeparatingTheDigitsInAnInteger {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		
		System.out.println("Please enter 5th digits number: ");
		int num = input.nextInt();
		
		int firstDigit = num / 10000;
		num = num % 10000;
		
		int secondDigit = num / 1000;
		num = num % 1000;	
		
		int thirdDigit = num / 100;
		num  = num % 100;
		
		int fourthDigit = num / 10;
		num = num % 10;
		
		System.out.printf("%d   %d   %d   %d   %d", firstDigit, secondDigit, thirdDigit, fourthDigit, num );
	}
}