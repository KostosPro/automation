package chapter2;

import java.util.Scanner;

public class Multiples {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Please enter first integer: ");
		int firstInt = input.nextInt();

		System.out.println("Please enter second integer: ");
		int secondInt = input.nextInt();

		int remainder = firstInt % secondInt;

		if (remainder == 0) {
			System.out.println("first integer is a multiple of the second. remainder = " + remainder);
			
		}else {
			System.out.println("first integer is NOT a multiple of the second. remainder = " + remainder);
		}

	}
}
