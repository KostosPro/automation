package chapter2;

import java.util.Scanner;

public class DiameterCircumferenceAndAreaOfaCircle {

	public static void main(String[] args) {

		double pi = Math.PI;
			
		Scanner input = new Scanner(System.in);
		
		System.out.println("Please enter a radius of a circle as integer: ");
		int radius = input.nextInt();
		input.close();
		
		System.out.printf("diameter = %d%n", 2 * radius);
		System.out.printf("circumference = %f%n", 2 * pi * radius);
		
		System.out.printf("area = %f%n", pi * Math.pow(radius, 2)); 
		// or we could use - System.out.printf("area = %f%n", pi * (radius * radius))
		
	}
}
