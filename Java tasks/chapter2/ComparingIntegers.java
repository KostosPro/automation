package chapter2;

import java.util.Scanner;

public class ComparingIntegers {
	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		int firstInt;
		int secondInt;
		
		System.out.println("Please enter first integer");
		firstInt = input.nextInt();
		
		System.out.println("Please enter second integer");
		secondInt = input.nextInt();
		
		if (firstInt > secondInt){
			System.out.printf("%d %s%n", firstInt, "is larger");
		}else if(firstInt == secondInt){
			System.out.printf("%s%n", "These numbers are equal");
		}else{
			System.out.printf("%d %s%n", secondInt, "is larger");
		}
	}
}
