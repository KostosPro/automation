package chapter2;

import java.util.Scanner;

public class NegativePositiveAndZeroValues {
	
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("enter first number");
		int num1 = input.nextInt();
		
		System.out.println("enter second number");
		int num2 = input.nextInt();
		
		System.out.println("enter third number");
		int num3 = input.nextInt();

		System.out.println("enter fourth number");
		int num4 = input.nextInt();
		
		System.out.println("enter fifth number");
		int num5 = input.nextInt();
		
		int amountOfNegatives = 0;
		int amountofPositeves = 0;
		int amountofZerous = 0;

//checking first number
		if (num1 > 0){
			amountofPositeves += 1;
		}else if (num1 == 0){
			amountofZerous += 1;
		}else{
			amountOfNegatives += 1;
		}

//checking second number
		if(num2 > 0){
			amountofPositeves += 1;
		}else if (num2 == 0){
			amountofZerous += 1;
		}else{
			amountOfNegatives += 1;
		}

// checking third number
		if(num3 > 0){
			amountofPositeves += 1;
		}else if (num3 == 0){
			amountofZerous += 1;
		}else{
			amountOfNegatives += 1;
		}

// checking fourth number
		if(num4 > 0){
			amountofPositeves += 1;
		}else if (num4 == 0){
			amountofZerous += 1;
		}else{
			amountOfNegatives += 1;
		}
		
//	checking fifth number
		
		if(num5 > 0){
			amountofPositeves += 1;
		}else if (num5 == 0){
			amountofZerous += 1;
		}else{
			amountOfNegatives += 1;
		}
		
		System.out.println("amount of positives = " + amountofPositeves);
		System.out.println("amount of negatives = " + amountOfNegatives);
		System.out.println("amount of zerous = " + amountofZerous);
	}
}
