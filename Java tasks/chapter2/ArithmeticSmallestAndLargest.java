package chapter2;

import java.util.Scanner;

public class ArithmeticSmallestAndLargest {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		int firstInt;
		int secondInt;
		int thirdInt;

		System.out.println("Please enter first integer");
		firstInt = input.nextInt();

		System.out.println("Please enter second integer");
		secondInt = input.nextInt();

		System.out.println("Please enter third integer");
		thirdInt = input.nextInt();

// counting sum		
		int sum = firstInt + secondInt + thirdInt;
		System.out.printf("%s = %d%n", "sum", sum);
	
// counting product		
		int product = firstInt * secondInt * thirdInt;
		System.out.printf("%s = %d%n", "product", product);
 
// counting average
		int average = (firstInt + secondInt + thirdInt) / 3;
		System.out.printf("%s = %d%n", "average", average);
		
// looking for a biggest number
		if (firstInt >= secondInt && firstInt >= thirdInt) {
			System.out.println("biggest = " + firstInt);
			
		}else if (secondInt >= firstInt && secondInt >= thirdInt) {
			System.out.println("biggest = " + secondInt);
			
		}else if (thirdInt >= firstInt && thirdInt >= secondInt) {
			System.out.println("biggest = " + thirdInt);
		}
		
//looking for a smallest number
		if (firstInt <= secondInt && firstInt <= thirdInt) {
			System.out.println("Smallest = " + firstInt);
			
		} else if (secondInt <= firstInt && secondInt <= thirdInt) {
			System.out.println("Smallest = " + secondInt);
			
		}else if (thirdInt <= firstInt && thirdInt <= secondInt){
			System.out.println("Smallest = " + thirdInt);
		}
	}
}