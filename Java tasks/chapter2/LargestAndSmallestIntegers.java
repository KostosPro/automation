package chapter2;

import java.util.Scanner;

public class LargestAndSmallestIntegers {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Please enter first integer: ");
		int num1 = input.nextInt();

		System.out.println("Please enter second integer: ");
		int num2 = input.nextInt();

		System.out.println("Please enter third integer: ");
		int num3 = input.nextInt();

		System.out.println("Please enter fourth integer: ");
		int num4 = input.nextInt();

		System.out.println("Please enter fifth integer: ");
		int num5 = input.nextInt();

		// looking for a largest integer
		
		if (num1 >= num2 && num1 >= num3 && num1 >= num4 && num1 >= num5) {
			System.out.println("largest number = " + num1);

		} else if (num2 >= num1 && num2 >= num3 && num2 >= num4 && num2 >= num5) {
			System.out.println("largest number = " + num2);

		} else if (num3 >= num1 && num3 >= num2 && num3 >= num4 && num3 >= num5) {
			System.out.println("largest number = " + num3);

		} else if (num4 >= num1 && num4 >= num2 && num4 >= num4 && num4 >= num5) {
			System.out.println("largest number = " + num4);

		} else if (num5 >= num1 && num5 >= num2 && num5 >= num3 && num5 >= num4) {
			System.out.println("largest number = " + num5);
		}

		// looking for a smallest integer

		if (num1 <= num2 && num1 <= num3 && num1 <= num4 && num1 <= num5) {
			System.out.println("Smallest integer: " + num1);

		} else if (num2 <= num1 && num2 <= num3 && num2 <= num4 && num2 <= num5) {
			System.out.println("Smallest integer: " + num2);

		} else if (num3 <= num1 && num3 <= num2 && num3 <= num4 && num3 <= num5) {
			System.out.println("Smallest integer: " + num3);

		} else if (num4 <= num1 && num4 <= num2 && num4 <= num3 && num4 <= num5) {
			System.out.println("Smallest integer: " + num4);

		} else if (num5 <= num1 && num5 <= num2 && num5 <= num3 && num5 <= num4) {
			System.out.println("Smallest integer: " + num5);

		}

	}
}