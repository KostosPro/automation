package chapter10;

public class Sphere13 extends ThreeDemensionalShape13 {

	private double radious;

	public Sphere13(double radious) {

		if (radious <= 0) {
			throw new IllegalArgumentException("radious should be > 0");
		}

		this.radious = radious;

	}

	public double getRadious() {
		return radious;
	}

	public void setRadious(double radious) {
		if (radious <= 0) {
			throw new IllegalArgumentException("radious should be > 0");
		}

		this.radious = radious;
	}

	@Override
	public double getArea() {
		return 4.0 * Math.PI * Math.pow(radious, 2);
	}

	@Override
	public double getVolume() {
		return 4.0 / 3 * Math.PI * Math.pow(getRadious(), 3);
	}

	@Override
	public String toString() {

		return String.format("\tSphere%nRadious = %.2f%n", getRadious());
	}

}
