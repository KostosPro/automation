package chapter10;

public class Tetrahedron13 extends ThreeDemensionalShape13 {

	private double edge;

	public Tetrahedron13(double edge) {

		if (edge <= 0) {

			throw new IllegalArgumentException("Tetrahedron edge should be > 0");
		}
		this.edge = edge;
	}

	public double getEdge() {
		return edge;
	}

	public void setEdge(double edge) {
		if (edge <= 0) {
			throw new IllegalArgumentException("Tetrahedron edge should be > 0");
		}

		this.edge = edge;
	}

	@Override
	public double getArea() {

		return Math.sqrt(3) * Math.pow(getEdge(), 2);
	}

	@Override
	public double getVolume() {
		return Math.sqrt(2) / 12 * Math.pow(edge, 3);
	}

	@Override
	public String toString() {

		return String.format("\tTetrahedron%nEdge = %.2f%n", getEdge());
	}

}
