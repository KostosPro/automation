package chapter10;

public class Circle13 extends TwoDemensionalShape13 {

	private double radious;

	public Circle13(double radious) {

		if (radious <= 0) {
			throw new IllegalArgumentException("radious should be > 0");
		}

		this.radious = radious;
	}

	public double getRadious() {
		return radious;
	}

	public void setRadious(double radious) {

		if (radious <= 0) {
			throw new IllegalArgumentException("radious should be > 0");
		}

		this.radious = radious;
	}

	@Override
	public double getArea() {
		return Math.PI * Math.pow(getRadious(), 2);
	}

	@Override
	public String toString() {

		return String.format("\tCircle%nRadious = %.2f%n", getRadious());
	}

}
