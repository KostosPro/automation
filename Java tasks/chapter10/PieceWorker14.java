package chapter10;

public class PieceWorker14 extends Employee {

	private double wagePerPiece;
	private double pieces;

	public PieceWorker14(String firstName, String lastName, String socialSecurityNumber, Date birthDay,
			double wagePerPiece, double pieces) {

		super(firstName, lastName, socialSecurityNumber, birthDay);

		setPieces(pieces);
		setWagePerPiece(wagePerPiece);

	}

	public double getWagePerPiece() {
		return wagePerPiece;
	}

	public void setWagePerPiece(double wagePerPiece) {

		if (wagePerPiece <= 0) {
			throw new IllegalArgumentException("Wage should be > 0");
		}

		this.wagePerPiece = wagePerPiece;
	}

	public double getPieces() {
		return pieces;
	}

	public void setPieces(double pieces) {

		if (pieces <= 0) {
			throw new IllegalArgumentException("Amount of Pieces should be > 0");
		}

		this.pieces = pieces;
	}

	@Override
	public double earnings() {
		return getWagePerPiece() * getPieces();
	}

	@Override
	public String toString() {

		return String.format("%s: %s%n%s: $%,.2f; %s: %.2f", "pieces employee", super.toString(), "wage per piece",
				getWagePerPiece(), "pieces", getPieces());

	}

}
