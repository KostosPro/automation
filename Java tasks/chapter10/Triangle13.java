package chapter10;

public class Triangle13 extends TwoDemensionalShape13 {

	private double hight;
	private double base;

	public Triangle13(double hight, double base) {

		if (hight <= 0) {
			throw new IllegalArgumentException("Triangle's hight should be > 0");
		}

		if (base <= 0) {
			throw new IllegalArgumentException("Triangle's base should be > 0");
		}

		this.hight = hight;
		this.base = base;
	}

	public double getHight() {
		return hight;
	}

	public void setHight(double hight) {
		if (hight <= 0) {
			throw new IllegalArgumentException("Triangle's hight should be > 0");
		}
		this.hight = hight;
	}

	public double getBase() {
		return base;
	}

	public void setBase(double base) {
		if (base <= 0) {
			throw new IllegalArgumentException("Triangle's base should be > 0");
		}

		this.base = base;
	}

	@Override
	public double getArea() {

		return 1.0 / 2 * getBase() * getHight();
	}

	@Override
	public String toString() {

		return String.format("\tTriangle%nHight = %.2f%nBase = %.2f%n", getHight(), getBase());
	}

}
