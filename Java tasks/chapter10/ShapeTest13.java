package chapter10;

public class ShapeTest13 {

	public static void main(String[] args) {

		Circle13 circle = new Circle13(3);
		Square13 square = new Square13(5);
		Triangle13 triangle = new Triangle13(5, 13);
		Cube13 cube = new Cube13(3);
		Sphere13 sphere = new Sphere13(4);
		Tetrahedron13 tetrahedron = new Tetrahedron13(4);

		Shape13[] shapes = { circle, square, triangle, cube, sphere, tetrahedron };

		for (Shape13 shape : shapes) {
			
			if (shape instanceof ThreeDemensionalShape13) {
				
				ThreeDemensionalShape13 threeDemShape = (ThreeDemensionalShape13) shape;
				System.out.printf("%sArea = %.2f%nVolume = %.2f%n", shape, shape.getArea(), threeDemShape.getVolume());

			} else {

				System.out.printf("%sArea = %.2f%n", shape, shape.getArea());
			}
		}
	}
}
