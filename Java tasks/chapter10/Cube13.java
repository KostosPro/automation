package chapter10;

public class Cube13 extends ThreeDemensionalShape13 {

	private double cubeSide;

	public Cube13(double cubeSide) {

		if (cubeSide <= 0) {
			throw new IllegalArgumentException("Cube side should be > 0");
		}

		this.cubeSide = cubeSide;
	}

	public double getCubeSide() {
		return cubeSide;
	}

	public void setCubeSide(double cubeSide) {
		if (cubeSide <= 0) {
			throw new IllegalArgumentException("Cube side should be > 0");
		}
		this.cubeSide = cubeSide;
	}

	@Override
	public double getArea() {

		return 6.0 * Math.pow(getCubeSide(), 2);
	}

	@Override
	public double getVolume() {

		return Math.pow(cubeSide, 3);
	}

	@Override
	public String toString() {

		return String.format("\tCube %nSide = %.2f%n", getCubeSide());
	}

}
