package chapter10;

public class Square13 extends TwoDemensionalShape13 {

	private double squareSide;

	public Square13(double squareSide) {

		if (squareSide <= 0) {
			throw new IllegalArgumentException("square side should be > 0");
		}

		this.squareSide = squareSide;
	}

	public double getSquareSide() {
		return squareSide;
	}

	public void setSquareSide(double squareSide) {
		if (squareSide <= 0) {
			throw new IllegalArgumentException("square side should be > 0");
		}

		this.squareSide = squareSide;
	}

	@Override
	public double getArea() {

		return Math.pow(getSquareSide(), 2);

	}

	@Override
	public String toString() {

		return String.format("\tSquare %nSide = %.2f%n", getSquareSide());

	}

}
