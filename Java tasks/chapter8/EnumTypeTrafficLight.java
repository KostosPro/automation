package chapter8;

public enum EnumTypeTrafficLight {
	
	RED(39),
	GREEN(45),
	YELLOW(15);
	
	private final int duration;
	
	EnumTypeTrafficLight(int duration){
		
	this.duration = duration;
		
	}

	public int getDuration() {
		return duration;
	}
	
	

}
