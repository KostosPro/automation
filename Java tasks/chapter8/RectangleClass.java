package chapter8;

public class RectangleClass {

	private double length = 1;
	private double width = 1;

	public double getLength() {
		return length;
	}

	public void setLength(double length) {

		if (length > 0 && length < 20) {
			this.length = length;

		} else {

			throw new IllegalArgumentException("length should be > 0 and < 20");
		}
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {

		if (width > 0 && width < 20) {
			this.width = width;

		} else {

			throw new IllegalArgumentException("Width should be > 0 and < 20");
		}
	}
	
 public double calculatePerimeter (){
	 
	 double perimeter = (length + width) * 2;
	 
	 return perimeter;
 }

 
 public double calculateArea(){
	 
	 double area = length * width;
	 
	 return area;
 }
}
