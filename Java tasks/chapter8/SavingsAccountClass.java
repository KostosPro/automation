package chapter8;

import java.math.BigDecimal;

import java.text.NumberFormat;

public class SavingsAccountClass {

	private static BigDecimal annualInterest;
	private BigDecimal savingsBalance;

	public static BigDecimal getAnnualInterest() {
		return annualInterest;
	}

	public BigDecimal getSavingsBalance() {
		return savingsBalance;
	}

	public void setSavingsBalance(BigDecimal savingsBalance) {
		this.savingsBalance = savingsBalance;
	}

	public BigDecimal calculateMonthlyInterest(BigDecimal annualInterest, BigDecimal savingsBalance) {

		savingsBalance = savingsBalance
				.add(savingsBalance.multiply(annualInterest).divide(new BigDecimal(12), 2, BigDecimal.ROUND_HALF_EVEN));

		return savingsBalance;

	}

	public static void annualInterestRate(BigDecimal annualInterest) {

		SavingsAccountClass.annualInterest = annualInterest;

	}

	@Override
	public String toString() {

		return String.format("%s", NumberFormat.getCurrencyInstance().format(getSavingsBalance()));

	}

}
