package chapter8;

public class EnumTypeTrafficLightTest {
	
	public static void main(String[] args){
		
		for(EnumTypeTrafficLight constant : EnumTypeTrafficLight.values()){
			
			System.out.printf("Duration of %s is %d seconds%n", constant, constant.getDuration());
		}
		
		
	}

}
