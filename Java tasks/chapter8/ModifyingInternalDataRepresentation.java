// Fig. 8.5: Time2.java
// Time2 class declaration with overloaded constructors.  
package chapter8;

public class ModifyingInternalDataRepresentation {
	private int seconds;

	// Time2 no-argument constructor:
	// initializes each instance variable to zero
	public ModifyingInternalDataRepresentation() {
		this(0, 0, 0); // invoke constructor with three arguments
	}

	// Time2 constructor: hour supplied, minute and second defaulted to 0
	public ModifyingInternalDataRepresentation(int hour) {
		this(hour, 0, 0); // invoke constructor with three arguments
	}

	// Time2 constructor: hour and minute supplied, second defaulted to 0
	public ModifyingInternalDataRepresentation(int hour, int minute) {
		this(hour, minute, 0); // invoke constructor with three arguments
	}

	// Time2 constructor: hour, minute and second supplied
	public ModifyingInternalDataRepresentation(int hour, int minute, int second) {
		setTime(hour, minute, second);
	}

	// Time2 constructor: another Time2 object supplied
	public ModifyingInternalDataRepresentation(ModifyingInternalDataRepresentation time) {
		// invoke constructor with three arguments
		this(time.getSeconds() / 3600, time.getSeconds() % 3600 / 60, time.getSeconds() % 60);
	}

	// Set Methods
	// set a new time value using universal time;
	// validate the data
	public void setTime(int hour, int minute, int second) {
		if (hour < 0 || hour >= 24)
			throw new IllegalArgumentException("hour must be 0-23");

		if (minute < 0 || minute >= 60)
			throw new IllegalArgumentException("minute must be 0-59");

		if (second < 0 || second >= 60)
			throw new IllegalArgumentException("second must be 0-59");

		seconds = hour * 3600 + minute * 60 + second;
	}

	// get second value
	public int getSeconds() {
		return seconds;
	}

	// convert to String in universal-time format (HH:MM:SS)
	public String toUniversalString() {
		return String.format("%02d:%02d:%02d", getSeconds() / 3600, getSeconds() % 3600 / 60, getSeconds() % 60);
	}

	// convert to String in standard-time format (H:MM:SS AM or PM)
	public String toString() {
		return String.format("%d:%02d:%02d %s", ((getSeconds() / 3600 == 0 || getSeconds() / 3600 == 12) ? 12 : (getSeconds() / 3600) % 12),
				 getSeconds() % 3600 / 60, getSeconds() % 60, (getSeconds() / 3600 < 12 ? "AM" : "PM"));
	}
} // end class Time2

/*
 * It would be perfectly reasonable for the Time2 class of Fig. 8.5 to represent
 * the time internally as the number of seconds since midnight rather than the
 * three integer values hour, minute and second. Clients could use the same
 * public methods and get the same results. Modify the Time2 class of Fig. 8.5
 * to implement the time as the number of seconds since midnight and show that
 * no change is visible to the clients of the class
 */
