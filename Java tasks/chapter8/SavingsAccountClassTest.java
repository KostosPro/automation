package chapter8;

import java.math.BigDecimal;

public class SavingsAccountClassTest {

	public static void main(String[] args) {

		SavingsAccountClass saver1 = new SavingsAccountClass();
		SavingsAccountClass saver2 = new SavingsAccountClass();

		saver1.setSavingsBalance(BigDecimal.valueOf(2000.00));
		saver2.setSavingsBalance(BigDecimal.valueOf(3000.00));

		SavingsAccountClass.annualInterestRate(BigDecimal.valueOf(0.04));
		System.out.println("When anual interest = 4%");
		
		calculateSavingPerMonths(saver1, 12);
		calculateSavingPerMonths(saver2, 12);
		System.out.printf("%nSaved amount of saver 1 = %s", saver1.toString());
		System.out.printf("%nSaved amount of saver 2 = %s", saver2.toString());

		SavingsAccountClass.annualInterestRate(BigDecimal.valueOf(0.05));
		System.out.println("\n\nWhen anual interest = 5%");

		
		calculateSavingPerMonths(saver1, 1);
		calculateSavingPerMonths(saver2, 1);
		System.out.printf("%nSavigns of saver 1 = %s", saver1);
		System.out.printf("%nSavigns of saver 2 = %s", saver2);

	}

	public static void calculateSavingPerMonths(SavingsAccountClass saver, int monthsQuantity) {

		if (monthsQuantity > 0 && monthsQuantity <= 12) {

			for (int i = 1; i <= monthsQuantity; i++) {

				saver.setSavingsBalance(saver.calculateMonthlyInterest(SavingsAccountClass.getAnnualInterest(),
						saver.getSavingsBalance()));

			}
		} else {
			throw new IllegalArgumentException("Quantity of motnths shoub be > 0 and <= 12");
		}
	}

}
