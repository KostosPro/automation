package chapter8;

import java.util.Scanner;

public class RectangleClassTest {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		RectangleClass rectangle = new RectangleClass();

		try {
			System.out.println("Please enter rectagle length");
			rectangle.setLength(input.nextDouble());
			
		} catch (IllegalArgumentException e) {

			System.out.println("EXEPTION!!! " + e.getMessage() + "\n");
		}
		
		try {
			System.out.println("Please enter rectangle width");
			rectangle.setWidth(input.nextDouble());
			
		} catch (IllegalArgumentException e) {

			System.out.println("EXEPTION!!! " + e.getMessage() + "\n");
		}

		System.out.printf("%nLength = %.2f; Widht = %.2f", rectangle.getLength(), rectangle.getWidth());
		
		System.out.printf("%nArea = %.2f", rectangle.calculateArea());
		System.out.printf("%nPerimetr = %.2f", rectangle.calculatePerimeter());
		
	}

}
