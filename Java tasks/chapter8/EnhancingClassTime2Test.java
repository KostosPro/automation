// Fig. 8.6: Time2Test.java
// Overloaded constructors used to initialize Time2 objects.
package chapter8;

public class EnhancingClassTime2Test {
	public static void main(String[] args) {
		
		EnhancingClassTime2 t1 = new EnhancingClassTime2(23, 59, 59);
		
		t1.tick();
		t1.incrementMinute();
		t1.incrementHour();
		
		displayTime("Incremented time", t1);

	}

	// displays a Time2 object in 24-hour and 12-hour formats
	private static void displayTime(String header, EnhancingClassTime2 t) {
		System.out.printf("%s%n   %s%n   %s%n", header, t.toUniversalString(), t.toString());
	}

} // end class Time2Test


