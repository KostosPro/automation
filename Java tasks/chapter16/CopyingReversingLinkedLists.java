package chapter16;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class CopyingReversingLinkedLists {
	
	public  static void main(String[] args){
		
		List<Integer> list1 = new LinkedList<>();
		List<Integer> list2 = new LinkedList<>();

		for(int i = 0; i < 10; i++){
			
			list1.add(i);
		}
		
		list2.addAll(list1);
		Collections.reverse(list2);
		
		System.out.println("List1 = " + list1.toString() + "\nList2 = " + list2.toString());
		
		
	}

}
