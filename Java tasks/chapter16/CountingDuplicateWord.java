package chapter16;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class CountingDuplicateWord {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		System.out.println("Please enter a sentence.");
		
		char[] charArray = scanner.nextLine().toCharArray();
		
		StringBuilder noPunctuationString = new StringBuilder(charArray.length);

		for(char one : charArray){
			
			if(Character.isLetter(one) || Character.isSpace(one)){ //getting rid of punctuation signs
				
				noPunctuationString.append(String.valueOf(one));
			}
		}
		
		String[] tokens = String.valueOf(noPunctuationString).split(" ");
		Set<String> stringSet = new HashSet<>();
		
		 int count = 0;
		 for(String token : tokens){
		
			 if(!(stringSet.add(token.toLowerCase()))){ // treating lower and upper case the same.
				 count++; // counting duplicates
			 }
		 }
		 System.out.printf("%n%d duplicated word/s in the sentence: \"%s\"", count, noPunctuationString);
	}

}
