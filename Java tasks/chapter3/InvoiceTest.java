package chapter3;

import java.util.Scanner;

public class InvoiceTest {

	public static void main(String[] args) {

		Invoice mouse = new Invoice("1022", "Mouse", 1, 15.00);
		Invoice notebook = new Invoice("2210", "Notebook", 1, 150.00);

		Scanner input = new Scanner(System.in);

		System.out.print("Please enter a quantyti of the sold Mouses: ");
		mouse.setQuantityOfItem(input.nextInt());

		System.out.printf("%nPlease enter a price per item for the sold Mouses: ");
		mouse.setPrice(input.nextDouble());

		System.out.printf("%nInvoice for the %d mouses is $%.2f", mouse.getQuantityOfItem(),
				mouse.getInvoceAmount(mouse.getQuantityOfItem(), mouse.getPrice()));

		System.out.println("\n***********************\n");

		System.out.print("Please enter a quantyti of the sold Notebooks: ");
		notebook.setQuantityOfItem(input.nextInt());

		System.out.printf("%nPlease enter a price per item for the sold Notebooks: ");
		notebook.setPrice(input.nextDouble());

		System.out.printf("%nInvoice for the %d notebooks is $%.2f", notebook.getQuantityOfItem(),
				notebook.getInvoceAmount(notebook.getQuantityOfItem(), notebook.getPrice()));

	}
}
