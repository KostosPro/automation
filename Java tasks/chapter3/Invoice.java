package chapter3;

public class Invoice {
	
	private String partNumber;
	private String partDescription;
	private int quantityOfItem;
	private double price;
	
	
// Initializing instance variables via Constructor
	public Invoice(String partNumber, String partDescription, int quantityOfItem, double price){
		
		this.partNumber = partNumber;
		this.partDescription = partDescription;
		this.quantityOfItem = quantityOfItem;
		this.price = price;
	}

// Creating getters and Setters 
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	
	public String getPartDescription() {
		return partDescription;
	}
	public void setPartDescription(String partDescription) {
		this.partDescription = partDescription;
	}
	
	public int getQuantityOfItem() {
		return quantityOfItem;
	}
	
	public void setQuantityOfItem(int quantityOfItem) {
		this.quantityOfItem = quantityOfItem;
	}
	
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
// creating getInvoceAmount method
	
	public double getInvoceAmount(int quantityOfItem, double price){
		
		if(quantityOfItem < 0){
			quantityOfItem = 0;
		}
		if(price < 0){
			price = 0.00;
		}
		
		double invoiceAmout = quantityOfItem * price;
				
		return invoiceAmout;	
	}
	

}
