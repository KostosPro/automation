package chapter3;

import java.util.Scanner;

public class EmployeeTest {

	public static void main(String[] args) {

//Creating objects
		Employee emp1 = new Employee("David", "Bush", 10);
		Employee emp2 = new Employee("Alice", "Smile", 2000);

// Displaying data from the constructor
		System.out.printf("First Emloyee%nName: %s, Last Name: %s, Month salary: %.2f%n%n", emp1.getName(),
				emp1.getLastName(), emp1.getMonthSalary());
		System.out.printf("Second Emloyee%nName: %s, Last Name: %s, Month salary: %.2f%n%n", emp2.getName(),
				emp2.getLastName(), emp2.getMonthSalary());

//Processing First Employee
		Scanner input = new Scanner(System.in);

		System.out.println("**Hello employee one**");

		System.out.println("Enter your Name: ");
		emp1.setName(input.nextLine());

		System.out.println("Enter your last Name: ");
		emp1.setLastName(input.nextLine());

		System.out.println("Enter your monthly salary");
		emp1.setMonthSalary(input.nextDouble());

		System.out.println("Your yearly salary is: $" + emp1.calculatingYearSalary(emp1.getMonthSalary()));
		System.out.println(
				"\nEnter an integer, it will be considered as a raise in percentage that you would like to get for yearly salary.");

		int raiseInPercentage = input.nextInt();
		System.out.printf("Your salary is raised with %d%% and now = $%.2f per year%n%n", raiseInPercentage,
				emp1.yearSalaryRaise(emp1.calculatingYearSalary(emp1.getMonthSalary()), raiseInPercentage));

// Processing second Employee
		System.out.println("**Hello employee two**");

		System.out.println("Enter your Name: ");
		input.nextLine();
		emp2.setName(input.nextLine());

		System.out.println("Enter your last Name: ");
		emp2.setLastName(input.nextLine());

		System.out.println("Enter your monthly salary");
		emp2.setMonthSalary(input.nextDouble());

		System.out.println("Your yearly salary is: $" + emp2.calculatingYearSalary(emp2.getMonthSalary()));
		System.out.println(
				"\nEnter an integer, it will be considered as a raise in percentage that you would like to get for yearly salary.");

		raiseInPercentage = input.nextInt();
		System.out.printf("Your salary is raised with %d%% and now = $%.2f per year%n%n", raiseInPercentage,
				emp2.yearSalaryRaise(emp2.calculatingYearSalary(emp2.getMonthSalary()), raiseInPercentage));

	}

}
