package chapter3;

public class Employee {
	private String name;
	private String lastName;
	private double monthSalary;

	public Employee(String name, String lastName, double salary) {

		this.name = name;
		this.lastName = lastName;

		if (salary >= 0) {
			this.monthSalary = salary;
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public double getMonthSalary() {
		return monthSalary;
	}

	public void setMonthSalary(double monthSalary) {

		if (monthSalary >= 0) {
			this.monthSalary = monthSalary;
		}
	}

	// method that will calculate year salary taking monthly as a parameter
	public double calculatingYearSalary(double monthSalary) {

		double salaryPerYear = monthSalary * 12;

		return salaryPerYear;
	}

	// method that raise year salary taking year salary and a percentage that it
	// should be raised with as the parameters.
	public double yearSalaryRaise(double salaryPerYear, double raisingPercentage) {

		salaryPerYear = salaryPerYear * (1 + raisingPercentage / 100);

		return salaryPerYear;
	}

}
