package chapter3;

import java.util.Scanner;

public class DateTest {

	public static void main(String[] args) {
		
		Date date = new Date(12,12,2018);
		date.displayDate(date.getMonth(), date.getDay(), date.getYear());
		
		Scanner input = new Scanner(System.in);
		
		System.out.printf("%nPlease enter month: ");
		date.setMonth(input.nextInt());
		
		System.out.printf("%nPlease enter day: ");
		date.setDay(input.nextInt());
		
		System.out.printf("%nPlease enter year: ");
		date.setYear(input.nextInt());
		
		date.displayDate(date.getMonth(), date.getDay(), date.getYear());

	}

}
