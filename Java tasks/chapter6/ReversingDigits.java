package chapter6;

import java.util.Scanner;

public class ReversingDigits {

	public static void main(String[] args) {

		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Please enterd a number. ");
		int num = input.nextInt();
		
		System.out.println("Reversed entered number = " + reverseNumber(num));
	}

	public static int reverseNumber(int num) {

		int reversedNum = 0;
		
		while (num != 0) {

			int remainder = num % 10;
			reversedNum = reversedNum * 10 + remainder;
			num = num / 10;

		}
		
		return reversedNum;
	}

}
