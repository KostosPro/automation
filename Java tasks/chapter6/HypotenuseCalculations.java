package chapter6;

public class HypotenuseCalculations {

	public static void main(String[] args) {
		
		System.out.println("Hypotenuse for the first triangle = " + hypotenuse(3, 4));
		System.out.println("Hypotenuse for the second triangle = " + hypotenuse(5, 12));
		System.out.println("Hypotenuse for the third triangle = " + Math.hypot(8, 15));

	}
	
	
	public static double hypotenuse(double sideA, double sideB){
		
		double hypotenuse = Math.pow(sideA, 2) + Math.pow(sideB, 2);
		hypotenuse = Math.sqrt(hypotenuse);
		
		return hypotenuse;		
		
	}

	
}
