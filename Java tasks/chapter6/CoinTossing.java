package chapter6;

import java.security.SecureRandom;
import java.util.Scanner;

public class CoinTossing {

	private enum Coin {
		HEADS, TAILS
	};

	public static void main(String[] args) {

		int headsCounter = 0;
		int tailsCounter = 0;

		while (makeChoice() == 1) {

			if (flip() == Coin.HEADS) {
				headsCounter++;

			} else {
				tailsCounter++;
			}

		}

		System.out.printf("You have got: %n\tHeads %d times %n\tTails %d times", headsCounter, tailsCounter);
	}

	public static Coin flip() {

		Coin coinSide;

		SecureRandom randomNum = new SecureRandom();

		if (randomNum.nextInt(2) == 0) {

			coinSide = Coin.HEADS;
			return coinSide;

		} else {

			coinSide = Coin.TAILS;
			return coinSide;

		}

	}

	// Method is created to avoid code duplication
	public static int makeChoice() {

		Scanner input = new Scanner(System.in);

		System.out.println("1)Please enter 1 to flip the coin");
		System.out.println("2)Please enter any other number to close the program");
		int choice = input.nextInt();

		return choice;
	}

}
