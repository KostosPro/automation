package chapter6;

import java.util.Scanner;

public class CircleArea {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Please enter radius: ");
		double radius = input.nextDouble();

		System.out.printf("Area of circle = %.2f", circleAreaCalculator(radius));
	}

	public static double circleAreaCalculator(double radius) {

		double area = 0;

		if (radius > 0) {

			area = Math.PI * Math.pow(radius, 2);
			return area;

		} else {
			System.out.println("ERROR radius cannot be Zero or less than Zero");
			return area;
		}

	}

}
