package chapter6;

import java.util.Scanner;

public class DisplayingASquareOfAnyCharacter {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Please enter square Length ");
		int squareLength = input.nextInt();

		System.out.println("Please enter character which will be used to display square");
		char fillCharacter = input.next().charAt(0);

		squareOfAnyCharacters(squareLength, fillCharacter);

	}

	public static void squareOfAnyCharacters(int squareLength, char fillCharacter) {

		if (squareLength > 0) {

			for (int i = 1; i <= squareLength; i++) {

				for (int j = 1; j <= squareLength; j++) {

					System.out.print(fillCharacter + " ");

				}

				System.out.println();

			}

		} else {
			 System.out.println("EROR - Square legth cannot be Zero or less than Zero");
		}

	}

}
