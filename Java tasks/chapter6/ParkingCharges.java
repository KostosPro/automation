package chapter6;

import java.util.Scanner;

public class ParkingCharges {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Please enter a quantity of customers:");
		int customersQuantity = input.nextInt(); // I use it instead of sentinel
													// statement
		double totalCheck = 0;

		if (customersQuantity > 0) {

			for (int i = 1; i <= customersQuantity;) {

				System.out.printf("%nPlease enter parking hours for custumer %d: ", i);
				double parkingHours = input.nextDouble();

				if (parkingHours <= 24 && parkingHours > 0) {

					double charge = calculateCharges(parkingHours);
					System.out.printf("%nCharge for customer %d is $%.2f", i, charge); // printing result for each customer

					totalCheck += charge; // calculating total

					i++; // increasing counter;

				} else {

					System.out.println("Parking hours cannot be > 24 and < 0. \nTry again.");

				}
			}

			System.out.println("\nTotal = $" + totalCheck); // displaying total
		}
	}

	public static double calculateCharges(double parkingHours) {

		double charge = 0;

		if (parkingHours > 0) {

			if (parkingHours <= 3) { // car was parked no more than 3 hours
										// charge 2$
				charge = 2;

			} else {
				charge = (parkingHours - 3) * 0.5 + 2; // if car parked more
														// than 3 hours count
														// additional charge.
			}

			if (charge > 10) { // Making sure that customer wont be charged more
								// than 10$ within 24 hours periodzF
				charge = 10;
			}
		}

		return charge;
	}
}
