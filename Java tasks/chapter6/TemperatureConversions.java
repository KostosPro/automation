package chapter6;

import java.util.Scanner;

public class TemperatureConversions {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Please enter 1 to get Celsius equivalent of Fahreinheit temperature");
		System.out.println("Please enter 2 to get Fahreinheit equivalent of Celsius temperature");
		int choice = input.nextInt();

		if (choice == 1) {

			System.out.println("\nPlease enter Fahreinheit temperature to get Celsius equivalent");
			double fahrenheit = input.nextDouble();
			
			System.out.printf("Celcius equivalent = %.2f", celsius(fahrenheit));

		} else if (choice == 2) {

			System.out.println("\nPlease enter Celcius temperature to get Fahreinheit equivalent");
			double celsius = input.nextDouble();
			
			System.out.printf("Fahreinheit equivalent = %.2f", fahrenheit(celsius));

		} else {
			
			System.out.println("Error wrong choice!!!");
		}
	}

	public static double celsius(double fahrenheit) {

		double celsiusEquivalent = 5.0 / 9.0 * (fahrenheit - 32);

		return celsiusEquivalent;

	}

	public static double fahrenheit(double celsius) {

		double fahrenheitEquivalent = 9.0 / 5.0 * celsius + 32;

		return fahrenheitEquivalent;

	}
}
