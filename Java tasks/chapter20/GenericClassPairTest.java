package chapter20;

public class GenericClassPairTest {

	public  static void main(String[] args){ 
		
		GenericClassPair<Double, Integer> doubleInt = new GenericClassPair<Double, Integer>(11.3, 12);
		GenericClassPair<String, Integer> stringInt = new GenericClassPair<String, Integer>("String", 12);
		
		System.out.println(doubleInt.getFirstElement() + " " + doubleInt.getSecondElemnt());
		System.out.println(stringInt.getFirstElement() + " " + stringInt.getSecondElemnt());

	}
	
}
