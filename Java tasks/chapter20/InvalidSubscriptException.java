package chapter20;

public class InvalidSubscriptException extends Exception {
	
	public InvalidSubscriptException(){
		
		super();
	}
	
public InvalidSubscriptException(String message){
		
		super(message);
	}

}
