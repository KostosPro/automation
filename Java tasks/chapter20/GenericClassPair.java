package chapter20;

public class GenericClassPair <F, S>{
		
	 private F firstElement;
	 private S secondElement;
	 
	 public GenericClassPair (F firstElement, S secondElement){
		 
		 setFirstElement(firstElement);
		 setSecondElemnt(secondElement);
	 }
	 
	public F getFirstElement() {
		return firstElement;
	}
	
	public void setFirstElement(F firstElement2) {
		this.firstElement = firstElement2;
	}
	
	public S getSecondElemnt() {
		return secondElement;
	}
	
	public void setSecondElemnt(S secondElement2) {
		this.secondElement = secondElement2;
	}
	
	 
	 
	
}
