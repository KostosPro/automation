// Fig. 20.3: GenericMethodTest.java
// Printing array elements using generic method printArray.
package chapter20;

import javax.lang.model.element.Element;

public class GenericMethodTest 
{
   public static void main(String[] args){
      // create arrays of Integer, Double and Character
      Integer[] integerArray = {1, 2, 3, 4, 5, 6};
      Double[] doubleArray = {1.1, 2.2, 3.3, 4.4, 5.5, 6.6, 7.7};
      Character[] characterArray = {'H', 'E', 'L', 'L', 'O'};
      String[] stringArray = {"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eigth"};

      System.out.print("Array integerArray contains:\n");
      printArray(integerArray); // pass an Integer array 
      try {
		printArray(integerArray, 3, 4);
	} catch (InvalidSubscriptException e) {
		e.printStackTrace();
	}
      
      System.out.printf("%nArray doubleArray contains:%n");
      printArray(doubleArray); // pass a Double array
      try {
  		printArray(doubleArray, 2, 5);
  	} catch (InvalidSubscriptException e) {
  		e.printStackTrace();
  	}
      System.out.printf("%nArray characterArray contains:%n");
      printArray(characterArray); // pass a Character array
      try {
  		printArray(characterArray, 0, 3);
  	} catch (InvalidSubscriptException e) {
  		e.printStackTrace();
  	}
      
      System.out.printf("%nArray stringArray contains:%n");
      printArray(stringArray);
      
   } 

   // generic method printArray
   public static <T> void printArray(T[] inputArray){
	   
      // display array elements
      for (T element : inputArray)
         System.out.printf("%s ", element);

      System.out.println();
   }
   
   //Overloaded method for 20.5
   
   public static <T> void printArray(T[] inputArray, int lowSubscript, int higthSubscript) throws InvalidSubscriptException{
	   
	   if(lowSubscript < 0){
		   
		   throw new InvalidSubscriptException("Index " + lowSubscript + " is out of range");
		   
	   } else if(higthSubscript > inputArray.length - 1){
		   
		   throw new InvalidSubscriptException("Index " + higthSubscript + " is out of range");
		   
	   }
	   
	   for(int i = lowSubscript; i <= higthSubscript; i++){
		   
		   System.out.printf("%s ", inputArray[i]);
	   }

	   System.out.println();
   }
   
   
   //Overloaded method for 20.6

   public static void printArray(String[] inputArray){
	   
	   for(int i = 0; i < inputArray.length; i++){
		   
		   if(inputArray.length / 2 == i){
			   System.out.println();
		   }
		   
		   System.out.printf("%s\t", inputArray[i]);
	   }
	   
	   }
   
} // end class GenericMethodTest

/**************************************************************************
 * (C) Copyright 1992-2014 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/