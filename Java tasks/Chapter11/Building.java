package Chapter11;

public class Building implements CarbonFootprint{
	
	private double usedPropan;
	
	public Building(double usedPropan){
		
		setUsedPropan(usedPropan);
		
	}

	public double getUsedPropan() {
		return usedPropan;
	}

	public void setUsedPropan(double usedPropan) {
		
		if(usedPropan < 0){
			throw new IllegalArgumentException("UsedPropan should be > 0");
		}
		
		this.usedPropan = usedPropan;
	}

	@Override
	public double getCarbonFootprint() {
		return getUsedPropan() * 1.5;
	}
	
	@Override
	public String toString(){
		
		return String.format("%n\tBuilding%nUsed Propan = %.2f", getUsedPropan());		
	}
	

}
