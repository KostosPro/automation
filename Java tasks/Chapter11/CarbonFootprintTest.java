package Chapter11;

import java.util.ArrayList;

public class CarbonFootprintTest {
	
	public static void main(String[] args){
		
		ArrayList<CarbonFootprint> carbonFootprints = new ArrayList();
	
		Car car = new Car(15);
		Building building = new Building(350);
		Bicycle bicycle = new Bicycle();
		
		carbonFootprints.add(car);
		carbonFootprints.add(building);
		carbonFootprints.add(bicycle);
		
		for(CarbonFootprint carbonFp : carbonFootprints){
			
			System.out.printf("%s%nCarbon Footprint = %.2f kg", carbonFp, carbonFp.getCarbonFootprint());
		}
		
		
	}

}
