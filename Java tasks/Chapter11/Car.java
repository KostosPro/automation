package Chapter11;

public class Car implements CarbonFootprint{

	private double usedPetrol;
	
	public Car(double usedPetrol){
		
		setUsedPetrol(usedPetrol);
		
	}

	public double getUsedPetrol() {
		return usedPetrol;
	}

	public void setUsedPetrol(double usedPetrol) {
		
		if(usedPetrol < 0){
			throw new IllegalArgumentException("Used Petrol should be > 0");			
		}
		
		this.usedPetrol = usedPetrol;
	}
	

	@Override
	public double getCarbonFootprint() {
		
		return getUsedPetrol() * 0.6;
	}
	
	@Override
	public String toString(){
		
	return String.format("%n\tCar%nUsed Petrol = %.2f", getUsedPetrol());
	}

}
