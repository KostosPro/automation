package Chapter11;

import chapter10.TwoDemensionalShape13;

public class ExceptionA extends Exception {

	ExceptionA() {

		super("Default Message - ExceptionA");
	}

	ExceptionA(String message) {
		super(message);
	}

	ExceptionA(String message, Throwable trow) {

		super(message, trow);
	}

	ExceptionA(Throwable trow) {

		super(trow);
	}

}
