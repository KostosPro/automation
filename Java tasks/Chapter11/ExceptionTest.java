package Chapter11;

import java.io.IOException;

public class ExceptionTest {

	public static void main(String[] args) {

		// 11.16

		try {
			throw new ExceptionB("Hi, I am Exception B");

		} catch (ExceptionA exception) {

			exception.printStackTrace();
		}

		try {
			throw new ExceptionC("Hi, I am Exception C");

		} catch (ExceptionA exception) {

			exception.printStackTrace();
		}

		// 11.17
		try {

			throw new ExceptionA();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		try {
			throw new ExceptionB();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		try {
			throw new NullPointerException();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		try {
			throw new IOException();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		try {
			someMethod();

		} catch (Exception exception) {
			exception.printStackTrace();
		}

	}

	//11.20

	public static void someMethod() throws Exception {

		{
			try {
				someMethod2();
			} catch (Exception ex) {

				throw new Exception("SomeMethod exception", ex);
			}
		}
	}

	public static void someMethod2() throws Exception {

		throw new Exception("SomeMethod exception");
	}

}
