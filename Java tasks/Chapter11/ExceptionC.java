package Chapter11;

public class ExceptionC extends ExceptionB {

	ExceptionC() {
		super("Default Message - ExceptionC");
	}

	ExceptionC(String message) {
		super(message);
	}

	ExceptionC(String message, Throwable trow) {
		super(message, trow);
	}

	ExceptionC(Throwable trow) {
		super(trow);
	}

}
