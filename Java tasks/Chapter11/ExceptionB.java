package Chapter11;

public class ExceptionB extends ExceptionA {
	
	ExceptionB() {

		super("Default Message - ExceptionB");
	}

	ExceptionB(String message) {
		super(message);
	}

	ExceptionB(String message, Throwable trow) {

		super(message, trow);
	}

	ExceptionB(Throwable trow) {
		super(trow);
	}
	

}
