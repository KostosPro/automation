package chapter4;

import java.util.Scanner;

public class SalaryCalculator {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Please enter worked hours last week: ");
		double hours = input.nextDouble();

		System.out.println("Please enter your rate per hour ");
		double ratePerHour = input.nextDouble();

		if (hours > 40) {
			System.out.printf("%nYour total salary is: $%.2f %nYour pay for overworked hours is: $%.2f",
					(ratePerHour * 40) + ((hours - 40) * 1.5), (hours - 40) * 1.5);
		} else {
			System.out.printf("%nYour salary is: $%.2f %nNo overworked hours pay", ratePerHour * hours);
		}
	}
}
