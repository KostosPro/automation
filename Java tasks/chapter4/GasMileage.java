package chapter4;

import java.util.Scanner;

public class GasMileage {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		double totalMiles = 0;
		double totalGallons = 0;
		int tripCount = 1;

		System.out.printf("Please enter the amount of Miles you drove during the trip %d, or type -1 to quite:%n",	tripCount);
		int miles = input.nextInt();

		while (miles != -1) {

			System.out.printf("Please enter the amount of Gallons that you used for the trip %d: %n", tripCount);
			int gallons = input.nextInt();

			System.out.printf("%nYou drove %.2f miles per 1 gallon%n", 1.00 * miles / gallons );

			totalMiles += miles;
			totalGallons += gallons;

			tripCount++;
			
			System.out.printf("%nPlease enter the amount of Miles you drove during the trip %d, or type -1 to quite: ",	tripCount);
			miles = input.nextInt();
		}
		
		
		System.out.printf("%nOn average you drove %.2f per 1 gallon", 1 * totalMiles / totalGallons);
	}

}
