package chapter4;

import java.util.Scanner;

public class Palindromes {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Please enter a five digits number: ");
		int enteredNum = input.nextInt();
		
		// Checking if entered number is 5 digits number
		// if it's not asking user to enter number again
		while(enteredNum / 10000 > 10 || enteredNum / 10000 <= 0 ) {
			System.out.println("Wrong enterence!!!!\nPlease Enter 5 digits number");
			enteredNum = input.nextInt();
		}


		int savedEnteredNum = enteredNum;
		int reversedNum = 0;

			while (enteredNum != 0) {

				int remainder = enteredNum % 10;
				reversedNum = reversedNum * 10 + remainder;
				enteredNum = enteredNum / 10;
			}
			
			if(savedEnteredNum == reversedNum){
				System.out.printf("%nNumber %d is the palindrom", savedEnteredNum);
			}else{
				System.out.printf("%nNumber %d is not the palindrom", savedEnteredNum);
			}
		} 

	}

