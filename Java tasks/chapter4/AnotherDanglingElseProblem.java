package chapter4;

public class AnotherDanglingElseProblem {

	public static void main(String[] args) {
		int y = 8;
		int x = 5;

		System.out.println("Variant A: ");
		// a)
		if (y == 8) {
			if (x == 5)
				System.out.println("@@@@@");

		} else {
			System.out.println("#####");
		}
		
		System.out.println("$$$$$");
		System.out.println("&&&&&");

		// b) and c); Is is okay that there are equal requirements for the
		// points b) and c)?

		System.out.println("\nVariant B and C: ");

		if (y == 8) {
			if (x == 5)
				System.out.println("@@@@@");

		} else {
			System.out.println("#####");
			System.out.println("$$$$$");
			System.out.println("&&&&&");
		}

		// d)
		
		System.out.println("\nVariant D: ");

		y = 7;

		if (y == 8) {
			if (x == 5)
				System.out.println("@@@@@");

		} else {
			System.out.println("#####");
			System.out.println("$$$$$");
			System.out.println("&&&&&");
		}
	}
}