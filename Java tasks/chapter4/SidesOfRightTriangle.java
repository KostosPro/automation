package chapter4;

import java.util.Scanner;

public class SidesOfRightTriangle {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Please Enter side a: ");
		int a = input.nextInt();

		System.out.println("Please Enter side b: ");
		int b = input.nextInt();

		System.out.println("Please Enter side c: ");
		int c = input.nextInt();

		if (a > 0 && b > 0 && c > 0) {
			
			if (Math.pow(a, 2) == Math.pow(b, 2) + Math.pow(c, 2)) {

				System.out.println("Entered numbers can represent the sides of a right triangle");

			} else if (Math.pow(b, 2) == Math.pow(a, 2) + Math.pow(c, 2)) {

				System.out.println("Entered numbers can represent the sides of a right triangle");

			} else if (Math.pow(c, 2) == Math.pow(a, 2) + Math.pow(b, 2)) {

				System.out.println("Entered numbers can represent the sides of a right triangle");
				
			}else {
				
				System.out.println("Entered numbers cannot represent the sides of a right triangle");
			}
			
		}else{

			System.out.println("Error wrong number has been entered!!!\nEntered Number should be > 0");
		}
	}

}
