package chapter4;

import java.util.Scanner;

public class PrintingTheDecimalEquivalentOfBinaryNumber {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Enter binary number: ");
		int binary = input.nextInt();
		
		int toMultiplyWith = 1;
		int digitalEquivalent = 0;
		
		while(binary != 0){
			
			int remainder = binary % 10; // taking last digit
			remainder = remainder * toMultiplyWith;	// multiply it with 1, 2, 4, 8 ...	
			digitalEquivalent += remainder; // adding result of multiplying 
			
			toMultiplyWith = toMultiplyWith * 2; // increasing multiplier
			binary = binary / 10;			
		}
		
		System.out.println("The decimal eqiuvalent of entered nusmber is: " + digitalEquivalent);
	}

}
