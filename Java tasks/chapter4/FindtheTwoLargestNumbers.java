package chapter4;

import java.util.Scanner;

public class FindtheTwoLargestNumbers {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		int counter = 1;
		int largest1 = 0;
		int largest2 = 0;

		while (counter <= 10) {

			System.out.printf("Please enter value with number: %d%n", counter);
			int number = input.nextInt();

			if (number > largest1) {

				largest2 = largest1;
				largest1 = number;
				
			}else if(number > largest2){
				largest2 = number;
			}
			
			counter++;
		}

		System.out.printf("%nFirst largest value is: %d", largest1);
		System.out.printf("%nSecond largest valuse is: %d", largest2);

	}
}