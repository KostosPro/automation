package chapter4;

import java.util.Scanner;

public class SquareOfAsterisks {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Enter square's side size: ");
		int squareLength = input.nextInt();

		if (squareLength > 0 && squareLength <= 20) {

			for (int i = 1; i <= squareLength; i++) {

				if (i == 1 || i == squareLength) {

					for (int count = 1; count <= squareLength; count++) {

						System.out.print("*"); // Printing top and bottom square
												// sides
					}

				} else {

					for (int count = 1; count <= squareLength; count++) {

						if (count == 1 || count == squareLength) {

							System.out.print("*"); // Printing left and right
													// square sides

						} else {

							System.out.print(" "); // making square hollow

						}
					}
				}
				System.out.println();
			}
			
		} else {
			System.out.println("Square size shoud be > 0 and <= 20");
		}
	}
}
