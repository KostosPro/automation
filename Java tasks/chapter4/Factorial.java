package chapter4;

import java.util.Scanner;

public class Factorial {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Please enter a number and you will get its factorial ");
		System.out.printf("%nFactorial = %.2f", calculateFactorial(input.nextInt())); // calculating and printing Factorial

		System.out.println("\nPlease enter a number of terms to calculate constant - E: ");	
		int numOfTerms = input.nextInt();
		double e = calculateE(numOfTerms); // saving calculated constant e in the variable so we can use it for "eInPow" method
		
		System.out.printf("%nConstant E = %.2f", e); // Printing constant E

		System.out.println("\nPlease enter a number of terms to calculate constant E in power: ");
		System.out.printf("%nConstant E in power = %.2f", eInPow(e, input.nextInt()));

	}

	public static double calculateFactorial(int factorial) {

		if (factorial > 0) {
			double stopNum = factorial;
			int counter = 1;

			while (counter < stopNum) {

				factorial *= counter;
				counter++;
			}

		} else if (factorial == 0) {

			factorial = 1;

		} else {
			System.out.println("Error Negative value is entered!!!");
			factorial = 0;
		}

		return factorial;

	}

	public static double calculateE(int numOfTerms) {

		int count = 1;
		double e = 1;

		if (numOfTerms <= 0) {

			System.out.println("Error Zero/Negative value has been entered, no calculations are made!!!");

		} else {
			
			while (count <= numOfTerms) {

				e = e + 1.00 / calculateFactorial(count);
				count++;
				
				System.out.printf("%nE = %.2f", e);
			}
		}
		return e;
	}

	public static double eInPow(double e, int numOfTerms) {

		int count = 1;
		double eInPow = e;

		if (numOfTerms <= 0){
			
			System.out.println("Error Zero/Negative value has been entered, no calculations are made!!!");

		}else{
			
		while (count < numOfTerms) {

			eInPow += (Math.pow(numOfTerms, count)) / calculateFactorial(numOfTerms);
			count++;
			}
		}
		return eInPow + 1;
	}

}
