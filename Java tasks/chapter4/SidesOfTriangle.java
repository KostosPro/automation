package chapter4;

import java.util.Scanner;

public class SidesOfTriangle {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Please Enter side a: ");
		int a = input.nextInt();

		System.out.println("Please Enter side b: ");
		int b = input.nextInt();

		System.out.println("Please Enter side c: ");
		int c = input.nextInt();

		if (a > 0 && b > 0 && c > 0) {
			if (a + b > c && a + c > b && b + c > a) {

				System.out.println("Entered sides can represent sides of a triangle");

			} else {
				System.out.println("Entered sides cannot represent sides of a triangle");
			}
		}else {
			System.out.println("Error wrong number has been entered!!!\nEntered Number should be > 0");
		}
	}
}
