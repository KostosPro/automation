package chapter4;

import java.util.Scanner;

public class SalesCommissionCalculator {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		double item1 = 239.99;
		double item2 = 129.75;
		double item3 = 99.95;
		double item4 = 350.89;

		double sumOfSoldItems = 0;
		double earning = 200;

		System.out.println("Please enter sold item:" + "\nEnter 1 if you sold first Item"
				+ "\nEnter 2 if you sold second Item" + "\nEnter 3 if you sold third Item"
				+ "\nEnter 4 if you sold fourth Item" + "\nEnter -1 if you FINISHED");
		int inputItem = input.nextInt();

		while (inputItem != -1) {

			if (inputItem == 1) {
				sumOfSoldItems += item1;

			} else if (inputItem == 2) {
				sumOfSoldItems += item2;

			} else if (inputItem == 3) {
				sumOfSoldItems += item3;

			} else if (inputItem == 4) {
				sumOfSoldItems += item4;

			} else {
				System.out.println("Unexisting Item number is entered!!!");
			}

			System.out.println("Please enter Next sold item number OR enter -1 if you have FINISHED to enter sold items");
			inputItem = input.nextInt();
		}
		
		if(sumOfSoldItems > 5000){
			
		System.out.printf("%nYour salary is: $%.2f", earning + (sumOfSoldItems / 100) * 9);
		
		} else {
			
			System.out.println("Your salary is = $ " + earning);
		}
	}

}
