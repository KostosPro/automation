package chapter4;

import java.util.Scanner;

public class CreditLimitCalculator {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Please enter Account Number OR -1 TO CLOSE THE PROGRAM: ");
		int accountNum = input.nextInt();

		while (accountNum != -1) {

			System.out.println("Please enter Beggining amount: ");
			int beginningBalance = input.nextInt();

			System.out.println("Please enter Charge amount: ");
			int charges = input.nextInt();

			if (charges >= 0) {

				System.out.println("Please enter Credit amount: ");
				int credit = input.nextInt();

				if (credit >= 0) {

					System.out.println("Please enter Credit Limit amount: ");
					int creditLimit = input.nextInt();

					if (creditLimit > 0) {

						int newBalance = (beginningBalance + charges) - credit;

						System.out.println("New balance = " + newBalance);

						if (newBalance < -creditLimit) {
							System.out.println("Credit Limit exeeded ");
						}
					} else {
						System.out.println("Credit Limit cannot be <= 0, Let's start from the beggining:");
					}
				} else {
					System.out.println("Credit cannot be < 0, Let's start from the beggining:");
				}

			} else {
				System.out.println("Charge cannot be < 0, Let's start from the beggining: ");
			}

			System.out.println("\nPlease enter Account Number OR -1 TO CLOSE THE PROGRAM: ");
			accountNum = input.nextInt();

		}

	}

}
