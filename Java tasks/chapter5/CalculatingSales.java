package chapter5;

import java.util.Scanner;

public class CalculatingSales {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Please enter product number OR enter -1 to close the program");
		int productNum = input.nextInt();
		
		double totalValue = 0;
		double productPrice = 0;


		while (productNum != -1) {

			if (productNum == 1 || productNum == 2 || productNum == 3 || productNum == 4 || productNum == 5) {

				System.out.println("Please enter sold quantity of the product:");
				int quantity = input.nextInt();

				if (quantity > 0) {

					switch (productNum) {

					case 1:
						productPrice = 2.98;
						break;
					case 2:
						productPrice = 4.50;
						break;
					case 3:
						productPrice = 9.98;
						break;
					case 4:
						productPrice = 4.49;
						break;
					case 5:
						productPrice = 6.87;
					}
					
					totalValue += productPrice * quantity;
					
				}else{
					System.out.println("Sold quantity cannot be Zero or less than Zero, Try again");
				}

			} else {
				System.out.println("Wrong product Number is entered, please Try again!");
			}

			System.out.println("Please enter product number OR enter -1 to close the program");
			productNum = input.nextInt();
			
		}
		
		System.out.printf("%nTotal retail value = $%.2f", totalValue);
	}

}
