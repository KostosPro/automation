package chapter5;

import java.util.Scanner;

public class BarChartPrintingProgram {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		int num1 = 0;
		int num2 = 0;
		int num3 = 0;
		int num4 = 0;
		int num5 = 0;

		int count = 1;
		while (count <= 5) {

			System.out.println("Plese enter a number that is > 1 and < 30: ");
			int num = input.nextInt();

			if (num > 1 && num < 30) {
				switch (count) { // assigning each number with corresponding entered value
				case 1:
					num1 = num;
					break;
				case 2:
					num2 = num;
					break;
				case 3:
					num3 = num;
					break;
				case 4:
					num4 = num;
					break;
				case 5:
					num5 = num;
				}

				count++;

			} else {
				System.out.println("Entered number does not met conditions!!! \nTry again");
			}
		}

		System.out.println("\n\tBAR CHART");
		
		for (int i = 1; i <= 5; i++) {

			switch (i) { // printing asterisks bars according to entered numbers

			case 1:
				for (int j = 1; j <= num1; j++) {
					System.out.print("*");
				}
				break;
			case 2:
				for (int j = 1; j <= num2; j++) {
					System.out.print("*");
				}
				break;
			case 3:
				for (int j = 1; j <= num3; j++) {
					System.out.print("*");
				}
				break;
			case 4:
				for (int j = 1; j <= num4; j++) {
					System.out.print("*");
				}
				break;
			case 5:
				for (int j = 1; j <= num5; j++) {
					System.out.print("*");
				}
				break;

			}

			System.out.println();

		}
	}

}
