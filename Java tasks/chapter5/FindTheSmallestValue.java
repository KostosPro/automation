package chapter5;

import java.util.Scanner;

public class FindTheSmallestValue {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Please enter amount of numbers that you are gonna enter: ");
		int amountOfEnterence = input.nextInt();

		if (amountOfEnterence > 1) {

			int count = 1;

			System.out.println("Please enter number " + count);
			int smallestNum = input.nextInt(); // considering first entered
												// number as smallest

			count++; // increasing count since first number already was entered

			while (count <= amountOfEnterence) {

				System.out.println("Please enter number " + count);
				int num = input.nextInt();

				if (smallestNum > num) {

					smallestNum = num;
				}

				count++;
			}

			System.out.println("\nThe smallest number is: " + smallestNum);

		} else {

			System.out.println("Amount of numbers cannot be less than 2");
		}
	}
}
