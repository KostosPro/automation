package chapter14;

import java.util.Scanner;

public class TokenizingComparingStrings {
	
	public static void main(String[] args){
		
		Scanner scanner = new Scanner(System.in);
		
		String textline = scanner.nextLine();
		
		String[] tokens = textline.split(" ");
		
		for(String token : tokens){
			
			if(token.endsWith("ED")){
				System.out.println(token);
			}
		}
		
	}

	
}
