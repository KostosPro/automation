package chapter14;

import java.security.SecureRandom;

public class RandomSentences {
	
	public static void main(String[] args){
		
		
		String[] articles = {"the", "a", "one", "some", "any"};
		String[] nouns = {"boy", "girl", "dog", "town", "car"};
		String[] verbs = { "drove", "jumped", "ran", "walked", "skipped"};
		String[] prepositions = { "to", "from", "over", "under", "on"};
		
		int i = 1;
		SecureRandom randomNum = new SecureRandom();
		StringBuilder sentence = new StringBuilder();

		while (i <= 20){
	
			sentence.delete(0, sentence.length());
			
			sentence.append(articles[randomNum.nextInt(5)]);
			sentence.append(" ");
			sentence.append(nouns[randomNum.nextInt(5)]);
			sentence.append(" ");
			sentence.append(verbs[randomNum.nextInt(5)]);
			sentence.append(" ");
			sentence.append(prepositions[randomNum.nextInt(5)]);
			sentence.append(" ");
			sentence.append(articles[randomNum.nextInt(5)]);
			sentence.append(" ");
			sentence.append(nouns[randomNum.nextInt(5)]);
			sentence.append(".");
			
			sentence.replace(0, 1, String.valueOf(sentence.charAt(0)).toUpperCase());
			

			System.out.printf("Senctence %d: %s%n", i, sentence);
			
			i++;
			
		}
		
	}

}
