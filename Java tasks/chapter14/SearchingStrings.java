package chapter14;

import java.util.Scanner;

public class SearchingStrings {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Please enter sentence:");
		String sentence = scanner.nextLine();
		
		System.out.println("Please enter character:");
		String character = scanner.nextLine();
		
		System.out.println("Character is met " + characterOccurenceCounter(sentence, character) + " times");
	}

	public static int characterOccurenceCounter(String sentence, String character) {

		int count = 0;

		for (int i = 0; i < sentence.length(); i++) {

			if (sentence.indexOf(character, i) != -1) {
				count++;
				i = sentence.indexOf(character, i);
			}

		}

		return count;
	}

}
