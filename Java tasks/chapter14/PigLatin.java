package chapter14;

import java.util.Scanner;

public class PigLatin {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		System.out.println("Please enter a sentence.");

		String sentence = scanner.nextLine();
		String[] tokens = sentence.split(" ");

		for (String token : tokens) {

			printLatinWorld(token);
		}
	}

	public static void printLatinWorld(String token) {

		System.out.printf(token.substring(1) + token.charAt(0) + "ay ");
	}
}
