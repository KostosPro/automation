package chapter14;

import java.util.Scanner;

public class MorseCode {

	public static void main(String[] args) {

		char[] charsAndDigits = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q',
				'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

		String[] morseAlphabet = { ".-", "-...", "-.-.", "-...", ".", "..-.", "--.", "....", "..", ".---", "-.-",
				".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--",
				"--..", ".----", "..---", "...--", "....-", ".....", "-....", "--...", "--...", "---..", "----.",
				"-----" };

		Scanner input = new Scanner(System.in);

		boolean isItTrue = true;

		while (isItTrue) {

			System.out.println(
					"Whan do you want to get?\nPress 1 to get Morse Code from phrase.\nPress 2 to get phrase from Morse Code. ");

			int choice = input.nextInt();

			if (choice == 1) {

				System.out.println("Please enter a phrase");
				input.nextLine();
				phraseToMorse(input.nextLine(), charsAndDigits, morseAlphabet);
				isItTrue = false;
			} else if (choice == 2) {

				System.out.println("Please enter Morse code");
				input.nextLine();
				morseToPhrase(input.nextLine(), charsAndDigits, morseAlphabet);
				isItTrue = false;

			} else {
				System.out.println("Try again");
			}
		}
	}

	public static void phraseToMorse(String phrase, char[] charsAndDigits, String[] morseAlphabet) {

		StringBuilder buider = new StringBuilder(phrase);

		char[] textToChars = new char[buider.length()];
		buider.getChars(0, buider.length(), textToChars, 0);

		for (char inputChar : textToChars) {

			for (int i = 0; i < charsAndDigits.length; i++) {

				if (String.valueOf(inputChar).equalsIgnoreCase(String.valueOf(charsAndDigits[i]))) {

					System.out.print(morseAlphabet[i] + " ");
					break;

				} else if (inputChar == ' ') {

					System.out.print("  ");
					break;
				}
			}
		}
	}

	public static void morseToPhrase(String phrase, char[] charsAndDigits, String[] morseAlphabet) {

		String[] byMorseWords = phrase.split("   ");

		for (String word : byMorseWords) {

			String[] byMorseLetters = word.split(" ");

			for (String letter : byMorseLetters) {

				for (int i = 0; i < morseAlphabet.length; i++) {

					if (letter.equals(morseAlphabet[i])) {
						System.out.print(charsAndDigits[i]);
						break;
					}
				}
			}
			System.out.print(" ");
		}
	}
}
