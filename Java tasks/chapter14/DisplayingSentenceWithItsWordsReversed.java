package chapter14;

import java.util.Scanner;

public class DisplayingSentenceWithItsWordsReversed {
	
	public static void main(String[] args){
		
		Scanner scanner = new Scanner(System.in);
		
		String someText = scanner.nextLine();
		System.out.println("Enter a sentence: ");
		
		String[] tokens = someText.split(" ");
		
		for(String word : tokens){
			
			for (int i = word.length() - 1; i >= 0; i--){
				
				System.out.print(word.charAt(i));
			}
			
			System.out.print(" ");
			
		}

		
	}

}
