package chapter14;

import java.util.Scanner;

public class PrintingDatesVariousFormats {
	
	public static void main(String[] args){
		
		Scanner scanner = new Scanner(System.in);
		
		String date = scanner.nextLine();
		
		if(date.matches("\\d{2}/\\d{2}/\\d{4}")){
			
			System.out.println(printDate(date));
		} else {
			System.err.println("Wrong date");
		}
	}
	
	public static String printDate(String date){
		
		String[] tokens = date.split("/");
		
		String month;
		
		switch(tokens[0]){
		case "01":
			month = "January";
			break;
		case "02":
			month = "February";
			break;
		case "03":
			month = "March";
			break;
		case "04":
			month = "April";
			break;
		case "05":
			month = "May";
			break;
		case "06":
			month = "June";
			break;
		case "07":
			month = "July";
			break;
		case "08":
			month = "August";
			break;
		case "09":
			month = "September";
			break;
		case "10":
			month = "October";
			break;
		case "11":
			month = "November";
			break;
		case "12":
			month = "December";
			break;
		default:
			month = null;
		
		}
		
		return String.format("%s %s, %s", month, tokens[1], tokens[2]);
	}

}
