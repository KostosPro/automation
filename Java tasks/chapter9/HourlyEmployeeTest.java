package chapter9;

public class HourlyEmployeeTest {
	
	public static void main (String[] args){
		
		HourlyEmployee employee = new HourlyEmployee("Dorina", "Pogor", "487364759", 10, 55);
		
		System.out.println("\tDisplaying using getters\n");
		System.out.println("First Name - " + employee.getFirstName());
		System.out.println("Last Name - " + employee.getLastName());
		System.out.println("SSN - " + employee.getSocialSecurityNumber());
		System.out.println("Hourly wage - " + employee.getWage());
		System.out.println("Worked hours - " + employee.getHours());
		System.out.println("Salary - " + employee.earnings(employee.getWage(), employee.getHours()));
		
		System.out.println("\n\n\tDisplaying using toString mehtod\n");
		System.out.println(employee.toString());
	}

}
