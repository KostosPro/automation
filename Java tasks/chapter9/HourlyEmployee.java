package chapter9;

public class HourlyEmployee extends Employee {

	private double hours;
	private double wage;

	public HourlyEmployee(String firstName, String lastName, String socialSecurityNumber, double wagePerHour,
			double hoursWorked) {

		super(firstName, lastName, socialSecurityNumber);

		setHours(hoursWorked);
		setWage(wagePerHour);

	}

	public double getHours() {
		return hours;
	}

	public void setHours(double hours) {

		if (hours < 0 || hours > 168) {

			throw new IllegalArgumentException("Hours caanot be < 0 and > 168");
		}
		this.hours = hours;
	}

	public double getWage() {
		return wage;
	}

	public void setWage(double wage) {

		if (wage < 0) {
			throw new IllegalArgumentException("Wage cannot be negative");
		}

		this.wage = wage;
	}

	public double earnings(double wagePerHor, double workedHours) {

		double salary;
		if (workedHours <= 40) {

			salary = wagePerHor * workedHours;

		} else {

			salary = 40 * wagePerHor + ((workedHours - 40) * wagePerHor * 1.5);
		}

		return salary;
	}

	@Override
	public String toString() {

		return String.format("%s %nWorked hours = %.2f %nWage per hour = %.2f %nSalary = $%.2f", super.toString(), wage,
				hours, earnings(getWage(), getHours()));
	}

}
