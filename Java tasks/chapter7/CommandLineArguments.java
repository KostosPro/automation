// Fig. 7.2: InitArray.java
// Initializing the elements of an array to default values of zero.
package chapter7;

public class CommandLineArguments {
	public static void main(String[] args) {
		int[] array;
		
		if (args.length != 0) {

			int getLength = Integer.parseInt(args[0]);

			array = new int[getLength];
			// int[] array = new int[Integer.parseInt(args[0])]; - hard core =)

		} else {
			
			array = new int[10];
		}
		
		System.out.printf("%s%8s%n", "Index", "Value"); // column headings

		// output each array element's value
		for (int counter = 0; counter < array.length; counter++) {
			System.out.printf("%5d%8d%n", counter, array[counter]);
		}
	}
} // end class InitArray
