package chapter7;

public class UsingEnhancedForStatement {

	public static void main(String[] args) {
		
		double total = 0;
		
		System.out.print("Command-line arguments: ");
		
		for (String element : args) {
			
			System.out.print(element + " ");
			total += Double.parseDouble(element);
		}
		
		System.out.println("\nTotal = " + total);
	}

}
