package chapter7;

import java.security.SecureRandom;

public class DiceRolling {

	public static void main(String[] args) {

		int[] possibleCombinations = { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
		int[] sumFrequency = new int[11];

		for (int i = 0; i < 36000000; i++) {

			int rollDice = rollDice();

			for (int j = 0; j < possibleCombinations.length; j++) {

				if (possibleCombinations[j] == rollDice) {

					++sumFrequency[j];
				}
			}
		}
		System.out.println("Possible combinations");
		
		for (int i = 0; i < possibleCombinations.length; i++) {
			
			System.out.printf("%15d", possibleCombinations[i]);
		}

		System.out.println("\n\nFrequency of each combination");

		for (int i = 0; i < sumFrequency.length; i++) {

			System.out.printf("%15d", sumFrequency[i]);
		}
	}
	public static int rollDice() {

		SecureRandom randomNum = new SecureRandom();

		int firstRoll = 1 + randomNum.nextInt(6);
		int secondRoll = 1 + randomNum.nextInt(6);
		int sum = firstRoll + secondRoll;
		
		return sum;
	}

}
