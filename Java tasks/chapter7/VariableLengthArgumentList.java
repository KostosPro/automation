package chapter7;

public class VariableLengthArgumentList {

	public static void main(String[] args) {
		
		int num1 = 10;
		int num2 = 100;
		int num3 = 1000;
		int num4 = 10000;
		
		System.out.println("First call = " + product(num1));
		System.out.println("Second call = " + product(num1, num2));
		System.out.println("Third call = " + product(num1, num2, num3));
		System.out.println("Fourth call = " + product(num1, num2, num3, num4));

	}

	public static int product(int... numbers) {
		int total = 0;

		for (int param : numbers) {

			total = total + param;
		}

		return total;
	}

}
