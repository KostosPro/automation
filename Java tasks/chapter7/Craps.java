// Fig. 6.8: Craps.java

// Craps class simulates the dice game craps.

/*
You roll two dice. Each die has six faces, which contain one, two, three, four, five and
six spots, respectively. After the dice have come to rest, the sum of the spots on the two
upward faces is calculated. If the sum is 7 or 11 on the first throw, you win. If the sum
is 2, 3 or 12 on the first throw (called �craps�), you lose (i.e., the �house� wins). If the
sum is 4, 5, 6, 8, 9 or 10 on the first throw, that sum becomes your �point.� To win,
you must continue rolling the dice until you �make your point� (i.e., roll that same
point value). You lose by rolling a 7 before making your point.
 */

package chapter7;

import java.security.SecureRandom;
import java.util.ArrayList;

public class Craps {
	// create secure random number generator for use in method rollDice
	private static final SecureRandom randomNumbers = new SecureRandom();

	// enum type with constants that represent the game status
	private enum Status {
		CONTINUE, WON, LOST
	};

	// constants that represent common rolls of the dice
	private static final int SNAKE_EYES = 2;
	private static final int TREY = 3;
	private static final int SEVEN = 7;
	private static final int YO_LEVEN = 11;
	private static final int BOX_CARS = 12;

	// plays one game of craps
	public static void main(String[] args) {

		ArrayList<Integer> winsList = new ArrayList<>();
		winsList.add(0); // adding first element so we can change it's value

		ArrayList<Integer> losesList = new ArrayList<>();
		losesList.add(0);

		int applicationRun = 100;
		int amountOfRolles = 0;
		int myPoint = 0; // point if no win or loss on first roll
		Status gameStatus; // can contain CONTINUE, WON or LOST

		for (int i = 0; i < applicationRun; i++) {

			int rollCounter = 0;
			int sumOfDice = rollDice(); // first roll of the dice
			amountOfRolles++;

			// determine game status and point based on first roll
			switch (sumOfDice) {
			case SEVEN: // win with 7 on first roll
			case YO_LEVEN: // win with 11 on first roll
				gameStatus = Status.WON;
				// in case won on first roll add 1 to first element
				winsList.set(rollCounter, winsList.get(rollCounter) + 1);

				break;
			case SNAKE_EYES: // lose with 2 on first roll
			case TREY: // lose with 3 on first roll
			case BOX_CARS: // lose with 12 on first roll
				gameStatus = Status.LOST;
				// in case lost on first roll add 1 to first element
				losesList.set(rollCounter, losesList.get(rollCounter) + 1);

				break;
			default: // did not win or lose, so remember point
				gameStatus = Status.CONTINUE; // game is not over
				myPoint = sumOfDice; // remember the point
				// System.out.printf("Point is %d%n", myPoint);
				break;
			}

			// while game is not complete
			while (gameStatus == Status.CONTINUE) // not WON or LOST
			{
				sumOfDice = rollDice(); // roll dice again
				rollCounter++;
				amountOfRolles++;

				// determine game status
				if (sumOfDice == myPoint) { // win by making point
					gameStatus = Status.WON;
					// making sure that there are enough elements in the
					// ArrayList
					if (winsList.size() <= rollCounter) {

						for (int j = 0; j <= rollCounter; j++) {
							winsList.add(0);
						}
					}
					// adding 1 to element according to roll number
					winsList.set(rollCounter, winsList.get(rollCounter) + 1);

				} else if (sumOfDice == SEVEN) {
					gameStatus = Status.LOST;

					if (losesList.size() <= rollCounter) {

						for (int j = 0; j <= rollCounter; j++) {
							losesList.add(0);
						}
					}
					losesList.set(rollCounter, losesList.get(rollCounter) + 1);
				}
			}
			// display won or lost message
			// if (gameStatus == Status.WON)
			// System.out.println("Player wins");
			// else
			// System.out.println("Player loses");
		}

		System.out.println("\tWINS");
		displayWinsOrLoses(winsList);

		System.out.println("\n\tLOSES");
		displayWinsOrLoses(losesList);

		// Looking for chance of winning
		System.out.println("\n\tChance of Winning");
		double winningChance = 0;

		for (int i = 0; i < winsList.size(); i++) {
			winningChance += winsList.get(i);
		}
		// displaying chance of winning
		System.out.printf( "%.2f%%", (winningChance * 100) / applicationRun);
		
		
		//displaying average
		System.out.println("\n\tAverage length\n" + amountOfRolles / applicationRun);

	}

	// roll dice, calculate sum and display results
	public static int rollDice() {
		// pick random die values
		int die1 = 1 + randomNumbers.nextInt(6); // first die roll
		int die2 = 1 + randomNumbers.nextInt(6); // second die roll

		int sum = die1 + die2; // sum of die values

		// display results of this roll
		// System.out.printf("Player rolled %d + %d = %d%n", die1, die2, sum);

		return sum;
	}

	public static void displayWinsOrLoses(ArrayList<Integer> arrayList) {

		for (int i = 1; i <= arrayList.size(); i++) {

			if (arrayList.get(i - 1) == 0) { // not displaying zero values
				continue;
			}
			// variable i, is set to 1 to track roll number that rather starts
			// from 1 than 0;
			System.out.printf("On the %d roll = %d %n", i, arrayList.get(i - 1));
		}
	}

} // end class Craps
