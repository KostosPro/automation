package chapter7;

import java.util.Scanner;

public class DuplicateElimination {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		int[] inputArray = new int[5];

		for (int i = 0; i < inputArray.length;) { // filling array according to
													// conditions

			System.out.println("Please enter " + (i + 1) + " nubmer. It should be between 10 and 100 inclusive");
			int num = input.nextInt();

			if (num >= 10 && num <= 100) {

				inputArray[i] = num;
				i++;

			} else {

				System.out.println("Error entered number should be >= 10 And <= 100! \nTry again. ");
			}
		}

		for (int i = 0; i < inputArray.length; i++) {

			int duplicateCounter = 0;

			for (int j = 0; j < inputArray.length; j++) {

				if (j == i) { // added condition that wont allow element to be
								// compared with itself
					continue;

				} else if (inputArray[i] == inputArray[j]) {

					duplicateCounter++;
				}
			}

			if (duplicateCounter == 0) {

				System.out.println("Entered unique number = " + inputArray[i]);

			}

		}

	}

}
