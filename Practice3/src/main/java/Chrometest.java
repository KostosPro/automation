import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Chrometest {

    public static void main(String[] args) {

        Workbook wb = new XSSFWorkbook();
        Sheet sheet = wb.createSheet();
        Row zeroRow = sheet.createRow(0);
        zeroRow.createCell(0).setCellValue("URL");
        zeroRow.createCell(1).setCellValue("Site Name");
        zeroRow.createCell(2).setCellValue("Occurrences");


        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);

        driver.get("http://www.google.com");

        WebElement element = driver.findElement(By.name("q"));
        element.sendKeys("bear");
        element.submit();

        List<WebElement> elements = driver.findElements(By.xpath("//div[h2[not(contains(text(),'People also as'))]]//div/a[h3]"));

        List<String> links = new ArrayList<String>();
        for (WebElement el : elements) {
            links.add(el.getAttribute("href"));
        }

        driver.findElement(By.xpath("//a[@aria-label=\"Page 2\"]")).click();
        links.add(driver.findElement(By.xpath("//div//a[h3]")).getAttribute("href"));


        for(int i = 0; i < links.size(); i++){

            driver.get(links.get(i));

            Row row = sheet.createRow(i+1);
            row.createCell(0).setCellValue(driver.getCurrentUrl());
            row.createCell(1).setCellValue(driver.getTitle());
            row.createCell(2).setCellValue(StringUtils.countMatches(driver.getPageSource().toLowerCase(),"bear"));
        }

        try (OutputStream fileOut = new FileOutputStream("workbook.xls")) {
            wb.write(fileOut);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
