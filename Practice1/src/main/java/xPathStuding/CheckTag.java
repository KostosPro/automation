package xPathStuding;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.IOException;

public class CheckTag {

    public static void main(String[] args) {


        File inputFile = new File("Company.xml");
        valuesBySpecifiedTag(inputFile, "//employee[@empId=111]");


    }

    public static void valuesBySpecifiedTag(File xmlFile, String expression) {

        try {

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder;
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);
            doc.getDocumentElement().normalize();
            XPath xPath = XPathFactory.newInstance().newXPath();

            Node node = (Node) xPath.compile(expression).evaluate(doc, XPathConstants.NODE);
            if (node != null) {

                if (node.hasChildNodes()) {
                    NodeList nodeList = node.getChildNodes();
                    for (int i = 0; null != nodeList && i < nodeList.getLength(); i++) {
                        Node nod = nodeList.item(i);
                        if (nod.getNodeType() == Node.ELEMENT_NODE) {
                            System.out.println(nodeList.item(i).getNodeName() + " : " + nod.getFirstChild().getNodeValue());
                        }
                        if(nod.hasChildNodes()){
                            NodeList nodList = nod.getChildNodes();
                            for(int count = 0; count < nodList.getLength(); count++){
                                Node thirdLvlNode = nodList.item(count);
                                if (thirdLvlNode.getNodeType() == Node.ELEMENT_NODE) {
                                    System.out.println(nodList.item(count).getNodeName() + " : " + thirdLvlNode.getFirstChild().getNodeValue());
                                }
                            }
                        }
                    }
                }
            }

        } catch (XPathExpressionException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

}
