package csvStunging;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class CreatingStudents2CommonsCSV {

    private static final String SAMPLE_CSV_FILE = "D:\\Git\\automation\\HomeWork\\Students2.csv";


    public static void main(String[] args) throws IOException {

        try (
                BufferedWriter writer = Files.newBufferedWriter(Paths.get(SAMPLE_CSV_FILE));

                CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT.withHeader("Num", "FullName","Mark", "University"));
        ) {
            csvPrinter.printRecord("1", "Sundar Pichai", "10", "USM");
            csvPrinter.printRecord("2", "Satya Nadella", "10", "UTM");
            csvPrinter.printRecord("3", "Tim cook", "10", "UTM");
            csvPrinter.printRecord("4", "Mark Zuckerberg", "5", "UASM");

            csvPrinter.flush();
        }

    }
}