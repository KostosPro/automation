package csvStunging;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ScannerCSVparser {

    public static void main(String[] args) {
        //Get scanner instance

        try (Scanner scanner = new Scanner(new File("Students2.csv"))) {

            //Set the delimiter used in file
            scanner.useDelimiter(",");

            //Get all tokens and store them in some data structure
            //I am just printing them
            while (scanner.hasNext()) {
                System.out.print(scanner.next() + "|");
            }

        } catch (Exception e) {
            e.printStackTrace();

        }
    }
}
