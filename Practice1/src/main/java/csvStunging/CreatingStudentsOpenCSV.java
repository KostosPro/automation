package csvStunging;

import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CreatingStudentsOpenCSV {

    public static void main(String[] args) {

        File file = new File("D:\\Git\\automation\\HomeWork\\Students.csv");

        try {
            // create FileWriter object with file as parameter
            FileWriter outputFile = new FileWriter(file);

            // create CSVWriter object filewriter object as parameter
            CSVWriter writer = new CSVWriter(outputFile);

            // create a List which contains String array
            List<String[]> data = new ArrayList<String[]>();
            data.add(new String[] { "FName", "LName", "Marks" });
            data.add(new String[] { "Aman", "Patian", "8.6" });
            data.add(new String[] { "Suraj", "Barashi", "8.7" });
            data.add(new String[] { "Makash", "Lavash", "6.7" });
            data.add(new String[] { "Givesh", "Shavermesh", "8.3" });
            writer.writeAll(data);

            // closing writer connection
            writer.close();
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }
}
