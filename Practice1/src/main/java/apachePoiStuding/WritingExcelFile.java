package apachePoiStuding;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;

public class WritingExcelFile {

    public static void main(String[] args) {

        try {
            File inputFile = new File("Company.xml");
            SAXBuilder saxBuilder = new SAXBuilder();
            Document document = saxBuilder.build(inputFile);
            System.out.println("Root element: " + document.getRootElement().getName());
            Element classElement = document.getRootElement();

            Workbook wb = new XSSFWorkbook();

            CellStyle firstRowStyle = wb.createCellStyle();
            Font font = wb.createFont();
            font.setBold(true);
            firstRowStyle.setFont(font);
            firstRowStyle.setAlignment(HorizontalAlignment.CENTER);
            firstRowStyle.setVerticalAlignment(VerticalAlignment.CENTER);

            CellStyle dateStyle = wb.createCellStyle();
            dateStyle.setDataFormat(wb.getCreationHelper().createDataFormat().getFormat("m/d/yyyy"));

            CellStyle styleWithWrap = wb.createCellStyle();
            styleWithWrap.setWrapText(true);
            styleWithWrap.setVerticalAlignment(VerticalAlignment.TOP);

            List<Element> groups = classElement.getChildren();

            for (int i = 0; i < groups.size(); i++) {

                Element group = groups.get(i);

                Sheet sheet = wb.createSheet(group.getAttributeValue("name") + " " + group.getAttributeValue("depId"));
                Row firstRow = sheet.createRow(0);

                firstRow.createCell(0).setCellValue("FirstName");
                firstRow.createCell(1).setCellValue("LastName");
                firstRow.createCell(2).setCellValue("Birth Date");
                firstRow.createCell(3).setCellValue("Position");
                firstRow.createCell(4).setCellValue("Teacher Id");
                firstRow.createCell(5).setCellValue("Skills");


                for (int j = 0; j < 6; j++) {
                    firstRow.getCell(j).setCellStyle(firstRowStyle);
                }

                List<Element> participants = group.getChildren();
                Element participant;
                for (int count = 0; count < participants.size(); count++) {

                    participant = participants.get(count);

                    Row nextRow = sheet.createRow(count + 1);

                    nextRow.createCell(0).setCellValue(participant.getChild("firstName").getValue());
                    nextRow.createCell(1).setCellValue(participant.getChild("lastName").getValue());

                    Cell birthDate = nextRow.createCell(2);
                    birthDate.setCellValue(new Date(participant.getChild("birthDate").getValue()));
                    birthDate.setCellStyle(dateStyle);

                    nextRow.createCell(3).setCellValue(participant.getChild("position").getValue());

                    if (participant.getChild("managerId") != null) {
                        nextRow.createCell(4).setCellValue(participant.getChild("managerId").getValue());
                    }

                    Cell skillsCell = nextRow.createCell(5);
                    skillsCell.setCellStyle(styleWithWrap);

                    List<Element> skills = participant.getChildren("skills");
                    Element skill;

                    for (int temp = 0; temp < skills.size(); temp++) {

                        skill = skills.get(temp);
                        skillsCell.setCellValue(skill.getValue().trim());
                    }
                }
            }
            OutputStream fileOut = new FileOutputStream("workbook.xls");
            wb.write(fileOut);

        } catch (JDOMException e) {
            e.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

    }


}
