package xmlStuding;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class QueryXmlRecursively {

    public static void main(String[] args) {

        String empId = "343";
        queryParticipant(empId);
    }

    public static void queryParticipant(String empId) {
        try {
            File inputFile = new File("company.xml");
            SAXBuilder saxBuilder = new SAXBuilder();
            Document document = saxBuilder.build(inputFile);
            Element classElement = document.getRootElement();

            List<Element> departments = classElement.getChildren();
            for (int temp = 0; temp < departments.size(); temp++) {

                List<Element> employees = departments.get(temp).getChildren();
                for (int count = 0; count < employees.size(); count++) {

                    if (employees.get(count).getAttributeValue("empId").equalsIgnoreCase(empId)) {

                        System.out.println("First Name: " + employees.get(count).getChild("firstName").getValue());
                        System.out.println("Last Name: " + employees.get(count).getChild("lastName").getValue());
                        System.out.println("Nick Name: " + employees.get(count).getChild("birthDate").getValue());
                        System.out.println("Position: " + employees.get(count).getChild("position").getValue());

                        Element skill = employees.get(count).getChild("Skills");
                        List<Element> skills = skill.getChildren();

                        for (int i = 0; i < skills.size(); i++) {

                            System.out.println("Skill: " + skills.get(i).getValue());
                        }

                        if (employees.get(count).getChild("managerId") != null) {
                            System.out.println("ManagerID : " + employees.get(count).getChild("managerId").getValue());
                            empId = employees.get(count).getChild("managerId").getValue();
                            System.out.println();
                            queryParticipant(empId);
                        }
                    }
                }
            }
        } catch (
                JDOMException e) {
            e.printStackTrace();
        } catch (
                IOException ioe) {
            ioe.printStackTrace();
        }
    }


}
