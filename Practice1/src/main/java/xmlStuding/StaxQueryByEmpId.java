package xmlStuding;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Iterator;

public class StaxQueryByEmpId {

    public static void main(String[] args) {

        try {
            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLEventReader eventReader =
                    factory.createXMLEventReader(new FileReader("Company.xml"));

            while (eventReader.hasNext()) {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement()) {
                    StartElement startElement = event.asStartElement();

                    if (startElement.getName().getLocalPart().equalsIgnoreCase("employee")) {

                        Iterator<Attribute> attributes = startElement.getAttributes();

                        if (attributes.next().getValue().equalsIgnoreCase("343")) {

                            while (eventReader.hasNext()) {
                                event = eventReader.nextEvent();

                                if (event.isStartElement()) {
                                    startElement = event.asStartElement();

                                    if (startElement.getName().getLocalPart().equalsIgnoreCase("managerId")) {
                                        event = eventReader.nextEvent();
                                        System.out.println("\nTeachers ID: " + event.asCharacters().getData());

                                    } else if (startElement.getName().getLocalPart().equalsIgnoreCase("firstName")) {
                                        event = eventReader.nextEvent();
                                        System.out.println("First Name: " + event.asCharacters().getData());

                                    } else if (startElement.getName().getLocalPart().equalsIgnoreCase("lastName")) {
                                        event = eventReader.nextEvent();
                                        System.out.println("Last Name: " + event.asCharacters().getData());

                                    } else if (startElement.getName().getLocalPart().equalsIgnoreCase("birthDate")) {
                                        event = eventReader.nextEvent();
                                        System.out.println("Birth Date: " + event.asCharacters().getData());

                                    } else if (startElement.getName().getLocalPart().equalsIgnoreCase("position")) {
                                        event = eventReader.nextEvent();
                                        System.out.println("Position: " + event.asCharacters().getData());

                                    } else if (startElement.getName().getLocalPart().equalsIgnoreCase("skill")) {
                                        event = eventReader.nextEvent();
                                        System.out.println("Skill: " + event.asCharacters().getData());
                                    }

                                } else if (event.isEndElement()) {
                                    if (event.asEndElement().getName().getLocalPart().equalsIgnoreCase("skills")) {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }

    }
}
