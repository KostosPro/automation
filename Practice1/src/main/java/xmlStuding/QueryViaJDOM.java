package xmlStuding;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class QueryViaJDOM {

    public static void main(String[] args) {

        try {
            File inputFile = new File("Company.xml");
            SAXBuilder saxBuilder = new SAXBuilder();
            Document document = saxBuilder.build(inputFile);
            Element classElement = document.getRootElement();

            List<Element> departments = classElement.getChildren();
            for (int temp = 0; temp < departments.size(); temp++) {

                List<Element> employee = departments.get(temp).getChildren();
                for (int count = 0; count < employee.size(); count++) {

                    if (employee.get(count).getAttributeValue("empId").equalsIgnoreCase("443")) {
                        System.out.println("First Name: " + employee.get(count).getChild("firstName").getValue());
                        System.out.println("Last Name: " + employee.get(count).getChild("lastName").getValue());
                        System.out.println("Nick Name: " + employee.get(count).getChild("birthDate").getValue());
                        System.out.println("Position: " + employee.get(count).getChild("position").getValue());

                        if (employee.get(count).getChild("managerId") != null) {
                            System.out.println("ManagerId: " + employee.get(count).getChild("managerId").getValue());
                        }
                        Element skill = employee.get(count).getChild("Skills");
                        List<Element> skills = skill.getChildren();

                        for (int i = 0; i < skills.size(); i++) {

                            System.out.println("Skill: " + skills.get(i).getValue());

                        }
                    }

                }
            }

        } catch (
                JDOMException e) {
            e.printStackTrace();
        } catch (
                IOException ioe) {
            ioe.printStackTrace();
        }

    }
}
