package xmlStuding;

import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class CreateXmlViaJdom {

    public static void main(String[] args) {

        try{
            File file = new File("D:\\Git\\automation\\HomeWork\\Company.xml");
            FileWriter fileWriter = new FileWriter(file);

            //root element
            Element kIndustry = new Element("KostosIndustry");
            Document doc = new Document(kIndustry);

            //First Department
            Element devDepartment = new Element("Departament");
            devDepartment.setAttribute(new Attribute("name","Development"));
            devDepartment.setAttribute(new Attribute("depId","1"));

            //First Employee
            Element emp1 = new Element("employee");
            emp1.setAttribute(new Attribute("empId","111"));

            Element emp1LastName = new Element("lastName");
            emp1LastName.setText("Sekar");

            Element emp1FirstName = new Element("firstName");
            emp1FirstName.setText("Gaurav");

            Element emp1DOB = new Element ("birthDate");
            emp1DOB.setText("01/01/2000");

            Element emp1Position = new Element("position");
            emp1Position.setText("Departament Manager");

            Element emp1Skills = new Element("Skills");

            Element emp1Skill = new Element("Skill");
            emp1Skill.setText("Communication");

            Element emp1Skill2 = new Element("Skill");
            emp1Skill2.setText("Hiring");

            emp1Skills.addContent(emp1Skill);
            emp1Skills.addContent(emp1Skill2);

            emp1.addContent(emp1LastName);
            emp1.addContent(emp1FirstName);
            emp1.addContent(emp1DOB);
            emp1.addContent(emp1Position);
            emp1.addContent(emp1Skills);

            //Second Employee

            Element emp2 = new Element("employee");
            emp2.setAttribute(new Attribute("empId","343"));

            Element emp2LastName = new Element("lastName");
            emp2LastName.setText("Pandey");

            Element emp2FirstName = new Element("firstName");
            emp2FirstName.setText("Pankaj");

            Element emp2DOB = new Element ("birthDate");
            emp2DOB.setText("01/01/2001");

            Element emp2Position = new Element("position");
            emp2Position.setText("Developer");

            Element emp2ManagerId = new Element("managerId");
            emp2ManagerId.setText("111");

            Element emp2Skills = new Element("Skills");

            Element emp2Skill = new Element("Skill");
            emp2Skill.setText("Drinking coffee ");

            Element emp2Skill2 = new Element("Skill");
            emp2Skill2.setText("Break code ");

            emp2Skills.addContent(emp2Skill);
            emp2Skills.addContent(emp2Skill2);

            emp2.addContent(emp2LastName);
            emp2.addContent(emp2FirstName);
            emp2.addContent(emp2DOB);
            emp2.addContent(emp2Position);
            emp2.addContent(emp2ManagerId);
            emp2.addContent(emp2Skills);

            devDepartment.addContent(emp1);
            devDepartment.addContent(emp2);

            //Second Department

            Element testDepartment = new Element("Departament");
            testDepartment.setAttribute(new Attribute("name","Testing"));
            testDepartment.setAttribute(new Attribute("depId","2"));

            //First Employee
            Element emp3 = new Element("employee");
            emp3.setAttribute(new Attribute("empId","222"));

            Element emp3LastName = new Element("lastName");
            emp3LastName.setText("Bucatov");

            Element emp3FirstName = new Element("firstName");
            emp3FirstName.setText("Bulat");

            Element emp3DOB = new Element ("birthDate");
            emp3DOB.setText("01/01/1997");

            Element emp3Position = new Element("position");
            emp3Position.setText("Departament Manager");

            Element emp3Skills = new Element("Skills");

            Element emp3Skill = new Element("Skill");
            emp3Skill.setText("Communication");

            Element emp3Skill2 = new Element("Skill");
            emp3Skill2.setText("Test environment preparing");

            emp3Skills.addContent(emp3Skill);
            emp3Skills.addContent(emp3Skill2);

            emp3.addContent(emp3LastName);
            emp3.addContent(emp3FirstName);
            emp3.addContent(emp3DOB);
            emp3.addContent(emp3Position);
            emp3.addContent(emp3Skills);

            //Second Employee

            Element emp4 = new Element("employee");
            emp4.setAttribute(new Attribute("empId","443"));

            Element emp4LastName = new Element("lastName");
            emp4LastName.setText("Humor");

            Element emp4FirstName = new Element("firstName");
            emp4FirstName.setText("Nancy");

            Element emp4DOB = new Element ("birthDate");
            emp4DOB.setText("01/01/1998");

            Element emp4Position = new Element("position");
            emp4Position.setText("QA Ingeneer");

            Element emp4ManagerId = new Element("managerId");
            emp4ManagerId.setText("222");

            Element emp4Skills = new Element("Skills");

            Element emp4Skill = new Element("Skill");
            emp4Skill.setText("Requirements analysis");

            Element emp4Skill2 = new Element("Skill");
            emp4Skill2.setText("Arguing with Devs");

            emp4Skills.addContent(emp4Skill);
            emp4Skills.addContent(emp4Skill2);

            emp4.addContent(emp4LastName);
            emp4.addContent(emp4FirstName);
            emp4.addContent(emp4DOB);
            emp4.addContent(emp4Position);
            emp4.addContent(emp4ManagerId);
            emp4.addContent(emp4Skills);

            testDepartment.addContent(emp3);
            testDepartment.addContent(emp4);

            doc.getRootElement().addContent(devDepartment);
            doc.getRootElement().addContent(testDepartment);

            XMLOutputter xmlOutput = new XMLOutputter();
            xmlOutput.setFormat(Format.getPrettyFormat());
            xmlOutput.output(doc, fileWriter);

        } catch(IOException e) {
            e.printStackTrace();
        }
    }
}
