package xmlStuding;

import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class parseViaJdom {

    public static void main(String[] args) {

        try {
            File inputFile = new File("Company.xml");
            SAXBuilder saxBuilder = new SAXBuilder();
            Document document = saxBuilder.build(inputFile);
            System.out.println("Root element :" + document.getRootElement().getName());
            Element classElement = document.getRootElement();

            List<Element> depList = classElement.getChildren();

            System.out.println("----------------------------");

            for (int temp = 0; temp < depList.size(); temp++) {

                Element departament = depList.get(temp);
                System.out.println("\nCurrent Element: "
                        + departament.getName());
                Attribute attrDepName = departament.getAttribute("name");
                Attribute attrDepID = departament.getAttribute("depId");

                System.out.println("Department: " + attrDepName.getValue());
                System.out.println("Department ID: " + attrDepID.getValue());

                List<Element> empList = departament.getChildren();
                for (int i = 0; i < empList.size(); i++) {

                    Element employee = empList.get(i);
                    System.out.println("\nCurrent Element: " + employee.getName());
                    System.out.println("Employee ID: " + employee.getAttribute("empId").getValue());
                    System.out.println("Last Name: " + employee.getChild("firstName").getValue());
                    System.out.println("First Name: " + employee.getChild("lastName").getValue());
                    System.out.println("DOB: " + employee.getChild("birthDate").getValue());
                    System.out.println("Position: " + employee.getChild("position").getValue());

                    List<Element> skillList = employee.getChild("Skills").getChildren();

                    for(int j = 0; j < skillList.size(); j++){

                        Element skill = skillList.get(j);
                        System.out.println("Skill: " + skill.getValue());
                    }
                }

                System.out.println("");

            }
        } catch (JDOMException e) {
            e.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

    }
}
