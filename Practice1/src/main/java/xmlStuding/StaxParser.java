package xmlStuding;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class StaxParser {

    public static void main(String[] args) {

        try {
            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLEventReader eventReader =
                    factory.createXMLEventReader(new FileReader("Company.xml"));

            while (eventReader.hasNext()) {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement()) {
                    StartElement startElement = event.asStartElement();

                    if (startElement.getName().getLocalPart().equalsIgnoreCase("Departament")) {
                        System.out.println("Start Element : Departament");

                        System.out.println(startElement.getAttributeByName(new QName("name")));
                        System.out.println(startElement.getAttributeByName(new QName("depId")));

                    } else if (startElement.getName().getLocalPart().equalsIgnoreCase("employee")) {
                        System.out.println("\nRoll No: " + startElement.getAttributeByName(new QName("empId")));

                    } else if (startElement.getName().getLocalPart().equalsIgnoreCase("managerId")) {
                        event = eventReader.nextEvent();
                        System.out.println(event.asCharacters().getData());
                    } else if (startElement.getName().getLocalPart().equalsIgnoreCase("firstname")) {
                        event = eventReader.nextEvent();
                        System.out.println(event.asCharacters().getData());
                    } else if (startElement.getName().getLocalPart().equalsIgnoreCase("lastname")) {
                        event = eventReader.nextEvent();
                        System.out.println(event.asCharacters().getData());
                    } else if (startElement.getName().getLocalPart().equalsIgnoreCase("birthDate")) {
                        event = eventReader.nextEvent();
                        System.out.println(event.asCharacters().getData());
                    } else if (startElement.getName().getLocalPart().equalsIgnoreCase("position")) {
                        event = eventReader.nextEvent();
                        System.out.println(event.asCharacters().getData());
                    } else if (startElement.getName().getLocalPart().equalsIgnoreCase("skill")) {
                        event = eventReader.nextEvent();
                        System.out.println(event.asCharacters().getData());
                    }

                }

                if (event.isEndElement()) {
                    EndElement endElement = event.asEndElement();

                    if (endElement.getName().getLocalPart().equalsIgnoreCase("group")) {
                        System.out.println("End Element : Group");
                        System.out.println();
                    }

                }

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }

    }
}
