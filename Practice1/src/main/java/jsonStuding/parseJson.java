package jsonStuding;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class parseJson {

    public static void main(String[] args) {

        JSONParser parser = new JSONParser();

        try {

            JSONObject company = (JSONObject) parser.parse(new FileReader("company.json"));
            JSONArray departments = (JSONArray) company.get("ITCompany");

            for (int temp = 0; temp < departments.size(); temp++) {

                JSONObject deparment = (JSONObject) departments.get(temp);
                JSONArray depEmployees = null;

                if (deparment.get("Development") != null) {
                    depEmployees = (JSONArray) deparment.get("Development");

                    System.out.println("Development Department Employees:");

                } else if (deparment.get("Testing") != null) {
                    depEmployees = (JSONArray) deparment.get("Testing");

                    System.out.println("Testing Department Employees:");
                }

                for (int i = 0; i < depEmployees.size(); i++) {

                    JSONObject employee = (JSONObject) depEmployees.get(i);
                    JSONObject employeeDetails = (JSONObject) employee.get("employee");


                    System.out.println("First Name: " + employeeDetails.get("firstName"));
                    System.out.println("Last Name: " + employeeDetails.get("lastName"));
                    System.out.println("Position: " + employeeDetails.get("position"));
                    System.out.println("Id: " + employeeDetails.get("id"));
                    System.out.println("Birth Date: " + employeeDetails.get("birthDate"));

                    if(employeeDetails.get("skills") != null){

                        JSONArray skills = (JSONArray) employeeDetails.get("skills");
                        for (Object skill: skills) {

                            System.out.println("Skill: " + skill);
                        }

                        System.out.println();

                    }

                }

                System.out.println("---------");
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }


    }

}
