package jsonStuding;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;

public class createJsonFile {

    public static void main(String[] args) {

        //Creating Dev department members

        //Manager
        JSONObject devDepManager = new JSONObject();

        devDepManager.put("id", "111");
        devDepManager.put("firstName", "Mister");
        devDepManager.put("lastName", "Snager");
        devDepManager.put("birthDate", "01.01.1997");
        devDepManager.put("position", "Manager");

        JSONArray devDepManagerSkills = new JSONArray();
        devDepManagerSkills.add("Hiring");
        devDepManagerSkills.add("Managing");

        devDepManager.put("skills", devDepManagerSkills);

        JSONObject devdepManagerObj = new JSONObject();
        devdepManagerObj.put("employee", devDepManager);


        //Employee
        JSONObject devDepEmployee = new JSONObject();
        devDepEmployee.put("id", "113");
        devDepEmployee.put("manager's Id", "111");
        devDepEmployee.put("fisrtName", "Mkyong");
        devDepEmployee.put("lastName", "King-kong");
        devDepEmployee.put("birthDate", "01.01.1999");
        devDepEmployee.put("position", "Developer");

        JSONArray devEmployeeSkills = new JSONArray();
        devEmployeeSkills.add("Sleeping");
        devEmployeeSkills.add("code breaking");

        devDepEmployee.put("skills", devEmployeeSkills);

        JSONObject devDepEmployeeObj = new JSONObject();
        devDepEmployeeObj.put("employee", devDepEmployee);


        //Creating Dev Department
        JSONArray devDepartmentEmployees = new JSONArray();
        devDepartmentEmployees.add(devdepManagerObj);
        devDepartmentEmployees.add(devDepEmployeeObj);

        JSONObject devDepartment = new JSONObject();
        devDepartment.put("Development", devDepartmentEmployees);


        //Creating Test department members

        //Manager
        JSONObject testDepManger = new JSONObject();

        testDepManger.put("id", "112");
        testDepManger.put("fisrtName", "Havana");
        testDepManger.put("lastName", "Liliana");
        testDepManger.put("birthDate", "01.01.1997");
        testDepManger.put("position", "Manager");

        JSONArray testDepManagerSkills = new JSONArray();
        testDepManagerSkills.add("Hiring");
        testDepManagerSkills.add("Managing");

        testDepManger.put("skills", testDepManagerSkills);

        JSONObject testDepMangerObj = new JSONObject();
        testDepMangerObj.put("employee", testDepManger);


        //Employee
        JSONObject testDepEmployee = new JSONObject();
        testDepEmployee.put("id", "113");
        testDepEmployee.put("manager's Id", "114");
        testDepEmployee.put("fisrtName", "Doul");
        testDepEmployee.put("lastName", "Kong");
        testDepEmployee.put("birthDate", "01.01.1999");
        testDepEmployee.put("position", "Tester");

        JSONArray testEmployeeSkills = new JSONArray();
        testEmployeeSkills.add("Sleeping");
        testEmployeeSkills.add("Code breaking");

        testDepEmployee.put("skills", testEmployeeSkills);

        JSONObject testDepEmployeeObj = new JSONObject();
        testDepEmployeeObj.put("employee", testDepEmployee);


        //Creating Test Department
        JSONArray testDepartmentEmployees = new JSONArray();
        testDepartmentEmployees.add(testDepMangerObj);
        testDepartmentEmployees.add(testDepEmployeeObj);

        JSONObject testDepartment = new JSONObject();
        testDepartment.put("Testing", testDepartmentEmployees);

        //Creating company
        JSONArray companyDepartments = new JSONArray();
        companyDepartments.add(devDepartment);
        companyDepartments.add(testDepartment);

        JSONObject company = new JSONObject();
        company.put("ITCompany", companyDepartments);

        try (FileWriter file = new FileWriter("company.json")) {
            file.write(company.toJSONString());
            file.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.print(company);

    }
}