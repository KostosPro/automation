import com.thoughtworks.xstream.XStream;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

public class BNMcurrencyToExcel {

    public static void main(String[] args) {

        String link = "https://bnm.md/en/official_exchange_rates?get_xml=1&date=";

        try (Scanner scanner = new Scanner(new File("Dates.csv"))) {

            Workbook wb = new XSSFWorkbook();
            Font font = wb.createFont();
            font.setBold(true);
            CellStyle cellStyle = wb.createCellStyle();
            cellStyle.setFont(font);


            scanner.useDelimiter(",");
            while (scanner.hasNext()) {

                String date = scanner.next();

                URL url = new URL(link + date);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Accept", "application/xml");

                if (conn.getResponseCode() != 200) {
                    throw new RuntimeException("Failed : HTTP error code : "
                            + conn.getResponseCode());
                }

                ValCurs valCurs = deserial(conn.getInputStream());

                Sheet sheet = wb.createSheet(date);
                Row zeroRow = sheet.createRow(0);


                zeroRow.createCell(0).setCellValue(valCurs.getDate());
                zeroRow.createCell(1).setCellValue(valCurs.getName());
                zeroRow.setRowStyle(cellStyle);

                int rowCounter = 2;
                for (int i = 0; i < valCurs.getValutes().size(); i++) {

                    Valute curVal = valCurs.getValutes().get(i);

                    Row firstRow = sheet.createRow(1);
                    Row row = sheet.createRow(rowCounter);

                    firstRow.setRowStyle(cellStyle);
                    firstRow.createCell(0).setCellValue("NumCode");
                    firstRow.createCell(1).setCellValue("CharCode");
                    firstRow.createCell(2).setCellValue("Nominal");
                    firstRow.createCell(3).setCellValue("Name");
                    firstRow.createCell(4).setCellValue("Value");
                    firstRow.createCell(5).setCellValue("Id");

                    row.createCell(0).setCellValue(curVal.getnumCode());
                    row.createCell(1).setCellValue(curVal.getСharCode());
                    row.createCell(2).setCellValue(curVal.getNominal());
                    row.createCell(3).setCellValue(curVal.getName());
                    row.createCell(4).setCellValue(curVal.getValue());
                    row.createCell(5).setCellValue(curVal.getId());

                    rowCounter++;

                }
            }

            OutputStream fileOut = new FileOutputStream("workbook.xls");
            wb.write(fileOut);

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    private static ValCurs deserial(InputStream servResponse) {
        XStream xstream = new XStream();
        xstream.processAnnotations(ValCurs.class);
        xstream.processAnnotations(Valute.class);

        ValCurs valCurs = (ValCurs) xstream.fromXML(servResponse);
        return valCurs;
    }
}
